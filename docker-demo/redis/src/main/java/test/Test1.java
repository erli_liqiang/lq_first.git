package test;

import org.junit.Test;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import java.util.LinkedList;
import java.util.List;

public class Test1 {

    @Test
    public  void  test1(){

        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        List<JedisShardInfo> shardInfos = new LinkedList<>();
        shardInfos.add(new JedisShardInfo("192.168.64.150",7000));
        shardInfos.add(new JedisShardInfo("192.168.64.150",7001));
        shardInfos.add(new JedisShardInfo("192.168.64.150",7002));

        ShardedJedisPool jedisPool = new ShardedJedisPool(jedisPoolConfig,shardInfos);
        ShardedJedis jedis = jedisPool.getResource();
        for (int i = 0; i < 100; i++) {
            jedis.set("key"+i,"value"+i);
        }
        System.out.println("存储完成");
    }

}
