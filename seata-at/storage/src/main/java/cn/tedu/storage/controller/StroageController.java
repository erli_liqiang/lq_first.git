package cn.tedu.storage.controller;

import cn.tedu.storage.service.StroageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StroageController {

    @Autowired
    private StroageService stroageService;


    @GetMapping("/decrease")
    public  String  decrease(Long productId, Integer count ){
        stroageService.decrease(productId,count);
        return  "减少库存成功";
    }


}
