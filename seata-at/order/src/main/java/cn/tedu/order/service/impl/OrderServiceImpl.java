package cn.tedu.order.service.impl;

import cn.tedu.order.entity.Order;
import cn.tedu.order.feign.AccountClient;
import cn.tedu.order.feign.EasyIdGeneratorClient;
import cn.tedu.order.feign.StroageClient;
import cn.tedu.order.mapper.OrderMapper;
import cn.tedu.order.service.OrderService;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private EasyIdGeneratorClient easyIdGeneratorClient;

    @Autowired
    private AccountClient accountClient;

    @Autowired
    private StroageClient stroageClient;
    // 1 创建TM  2.连接TC 3.注册全局事务 向TC注册
    @GlobalTransactional //全局事务
    @Override
    public void create(Order order) {
        // TODO: 从全局唯一id发号器获得id，这里暂时随机产生一个 orderId
        Long orderId = easyIdGeneratorClient.nextId("order_business");

        //Long orderId = Long.valueOf(new Random().nextInt(Integer.MAX_VALUE));
        order.setId(orderId);

        orderMapper.create(order);

        // TODO: 调用storage，修改库存
        stroageClient.decrease(order.getProductId(),order.getCount());


        // TODO: 调用account，修改账户余额
        // 修改账户余额
        accountClient.decrease(order.getUserId(), order.getMoney());
    }
}
