package demo7;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

//消费者
public class Consumer {
    public static void main(String[] args) throws MQClientException {

        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("consumer-demo7");
        consumer.setNamesrvAddr("192.168.64.141:9876");
        consumer.subscribe("Topic6","*");

        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                for (MessageExt ext : list) {
                    String msg = new String(ext.getBody());
                    System.out.println("收到"+msg);
                }
                if (Math.random()<0.5){
                    System.out.println("消息处理成功");
                    return  ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
                }else {
                    System.out.println("消息处理失败，稍后重试");
                    return  ConsumeConcurrentlyStatus.RECONSUME_LATER;
                }

            }
        });

        consumer.start();
        System.out.println("开始处理消息");
    }
}
