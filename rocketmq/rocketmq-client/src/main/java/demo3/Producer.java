package demo3;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.util.Scanner;

//单向消息 生产这个
public class Producer {

    public static void main(String[] args) throws MQClientException, RemotingException, InterruptedException {

        DefaultMQProducer producer = new DefaultMQProducer("producer-demo3");
        producer.setNamesrvAddr("192.168.64.141:9876");
        producer.start();

        //发送消息
        while (true){
            System.out.println("输入消息");
            String s = new Scanner(System.in).nextLine();
            Message message = new Message("Topic2", "TagA", s.getBytes());
            producer.sendOneway(message);

        }

    }


}
