package demo6;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.util.Random;
import java.util.Scanner;

//消息过滤
public class Producer {

    public static void main(String[] args) throws MQClientException, RemotingException, InterruptedException, MQBrokerException {

        DefaultMQProducer producer = new DefaultMQProducer("producer-demo6");
        producer.setNamesrvAddr("192.168.64.141:9876");


        producer.start();

        while (true){
            System.out.println("输入消息");
            String s = new Scanner(System.in).nextLine();
            Message msg = new Message("Topic5", s.getBytes());
            msg.putUserProperty("rnd",""+new Random().nextInt(4));
        producer.send(msg);
        }
    }
}
