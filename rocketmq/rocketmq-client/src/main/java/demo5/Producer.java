package demo5;


import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.util.Scanner;

// 延时消息 生产者
public class Producer {
    public static void main(String[] args) throws MQClientException, RemotingException, InterruptedException, MQBrokerException {

        DefaultMQProducer producer = new DefaultMQProducer("producer-demo5");

        producer.setNamesrvAddr("192.168.64.141:9876");

        producer.start();

        while (true){
            System.out.println("输入消息");
            String s = new Scanner(System.in).nextLine();
            Message message = new Message("Topic4", "*",s.getBytes());

            message.setDelayTimeLevel(3);
            producer.send(message);

        }

    }
}
