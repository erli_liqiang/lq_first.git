package demo4;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.util.List;
import java.util.Scanner;

//顺序消息 生产者
public class Producer {
    static String[] msgs = {
            "15103111039,创建",
            "15103111065,创建",
            "15103111039,付款",
            "15103117235,创建",
            "15103111065,付款",
            "15103117235,付款",
            "15103111065,完成",
            "15103111039,推送",
            "15103117235,完成",
            "15103111039,完成"
    };

    public static void main(String[] args) throws MQClientException, RemotingException, InterruptedException, MQBrokerException {

        DefaultMQProducer producer = new DefaultMQProducer("producer-demo4");

        producer.setNamesrvAddr("192.168.64.141:9876");
        producer.start();

        for (String s : msgs) {
            System.out.println("按回车之后发送消息"+s);
            new Scanner(System.in).nextLine();
            Message message = new Message("Topic3", "TagA", s.getBytes());

            String sid = s.split(",")[0];
            long id = Long.parseLong(sid);
            SendResult r = producer.send(message, new MessageQueueSelector() {
                @Override
                public MessageQueue select(List<MessageQueue> list, Message message, Object o) {

                    long id = (long) o;
                    int index = (int) (id % list.size());
                    System.out.println("消息已发送到: " + list.get((int) index));
                    return list.get(index);
                }
            }, id);

            System.out.println("-------------------------"+r);
        }
    }
}
