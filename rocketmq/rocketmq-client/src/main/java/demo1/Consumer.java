package demo1;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

//消费者
public class Consumer {
    public static void main(String[] args) throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("consumer-demo1");

        //
        consumer.setNamesrvAddr("192.168.64.141:9876");
        //处理消息

        // * 表示接收多个标签
        consumer.subscribe("Topic1","TagA || TagB");

        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                for (MessageExt msg : list) {
                    String string = new String(msg.getBody());
                    System.out.println("收到 "+string);

                }

                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS; // 消费成功
//                return  ConsumeConcurrentlyStatus.RECONSUME_LATER; // 稍后再次重试
            }
        });

        consumer.start();
        System.out.println("开始消费数据");


    }
}
