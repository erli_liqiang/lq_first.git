package demo1;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.util.Scanner;

//生产者
public class Producer {


    public static void main(String[] args) throws MQClientException, RemotingException, InterruptedException, MQBrokerException {
        //生产者组  防止生产者死掉 可以向其他生产者反馈
        DefaultMQProducer producer = new DefaultMQProducer("producer-demo1");
        //若是多太服务器，则用冒号隔开写多个地址
        producer.setNamesrvAddr("192.168.64.141:9876");
        //启动生产者
        producer.start();

        //主题相当于是消息的分类, 一类消息使用一个主题
        String  topic = "Topic1";

        // tag 相当于是消息的二级分类, 在一个主题下, 可以通过 tag 再对消息进行分类
        String  tag = "TagA";

        while (true){
            System.out.println("输入消息 用逗号分割多条消息");
            String input = new Scanner(System.in).nextLine();
            String[] a = input.split(",");
            for (String s : a) {
                Message message = new Message(topic, tag, s.getBytes());
                SendResult result = producer.send(message);
                System.out.println("反馈结果："+result);
            }

        }
    }
}
