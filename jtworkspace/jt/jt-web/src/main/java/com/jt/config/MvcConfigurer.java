package com.jt.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
@Configuration
public class MvcConfigurer implements WebMvcConfigurer {
	//开启后缀匹配设置
	@Override
	public void configurePathMatch(PathMatchConfigurer configurer) {
		//相当于web.xml的配置
		configurer.setUseSuffixPatternMatch(true);
	}
}
