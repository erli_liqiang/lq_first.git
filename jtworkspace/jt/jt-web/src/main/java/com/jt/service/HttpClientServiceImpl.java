package com.jt.service;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Service;

import com.jt.pojo.User;

@Service
public class HttpClientServiceImpl implements HttpClientService {
	//http://sso.jt.com/user/httpClient/saveUser?
	@Override
	public void saveUser(User user) {
	
		String url  = "http://sso.jt.com/user/httpClient/saveUser?username="
		+user.getUsername()+"&password="+user.getPassword();
		CloseableHttpClient httpClient = HttpClients.createDefault();
		//定义请求方式
		HttpGet get = new HttpGet(url);
		try {
			CloseableHttpResponse response = httpClient.execute(get);
			
			if (response.getStatusLine().getStatusCode()!=200) {
				throw new RuntimeException("请求错误");
			}
		}catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("请求错误");
		}
		
		
		
		
	}
}
