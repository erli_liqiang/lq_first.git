package com.jt.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class UserController {
	
	//实现页面的通用跳转
	@RequestMapping("/{moduleName}")
	public String toPage(@PathVariable("moduleName")String moduleName) {
		return moduleName;
		
	}
	
}
