package com.jt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jt.pojo.User;
import com.jt.service.HttpClientService;
@RestController
public class HttpClientController {
	@Autowired
	private HttpClientService httpClientService;
	//测试数据http://www.jt.com/user/httpClient/saveUser/admin1234/123
	// http://www.jt.com/user/httpClient/saveUser/{username}/{password}
	@RequestMapping("/user/httpClient/saveUser/{username}/{password}")
	public String  saveUser(User user) {
		httpClientService.saveUser(user);
		return "httpClient测试成功";
	}
}
