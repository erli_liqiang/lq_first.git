package com.jt.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import redis.clients.jedis.Jedis;

@SpringBootTest
public class TestRedis {
	@Autowired
	private Jedis jedis;
	@Test
	public void test01() {
		
		jedis.set("ppx", "我们走");
		String json = jedis.get("ppx");
		System.out.println(json);
	}
	
}
