package com.jt.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.jt.pojo.ItemDesc;

@RestController
public class JSONPController {
	
	
	@RequestMapping("/web/testJSONP")
	public JSONPObject jsonp(String callback) {
		ItemDesc itemDesc = new ItemDesc();
		itemDesc.setItemId(100l);
		itemDesc.setItemDesc("我是商品详情");
		return new JSONPObject(callback, itemDesc);
		
	}
//	public String jsonp(String callback) {
//		return callback+"({'id':'100','name':'tomcat'})";
//		
//	}
	
}
