package com.jt.service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.ItemDescMapper;
import com.jt.mapper.ItemMapper;
import com.jt.mapper.itemCatMapper;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.vo.EasyUITable;

@Service
public class ItemServiceImpl implements ItemService {
	
	@Autowired
	private ItemMapper itemMapper;
	@Autowired
	private ItemDescMapper itemDescMapper;
	//商品上下架操作
	@Override
	public void updateStatus(Long[] ids,Integer i) {
		List<Long> idsList = Arrays.asList(ids);
		UpdateWrapper<Item> updateWrapper = new UpdateWrapper<>();
		updateWrapper.in("id", idsList);
		Item item = new Item();
		item.setStatus(i);
		itemMapper.update(item, updateWrapper);
		
	}
	
	//保存操作
	@Transactional
	@Override
	public void saveItem(Item item,ItemDesc itemDesc) {
		item.setStatus(1).setCreated(new Date()).setUpdated(item.getCreated());
		itemMapper.insert(item);
		itemDesc.setItemId(item.getId());
		itemDescMapper.insert(itemDesc);
		
	}
	//修改操作
	@Transactional
	@Override
	public void updateItem(Item item,ItemDesc itemDesc) {
		item.setStatus(1).setUpdated(new Date());
		itemMapper.updateById(item);
		itemDesc.setItemId(item.getId());
		itemDescMapper.updateById(itemDesc);
		
	}
	//删除操作
	@Transactional
	@Override
	public void deleteItems(Long[] ids) {
		List<Long> idList = Arrays.asList(ids);     
		itemMapper.deleteBatchIds(idList);
		itemDescMapper.deleteBatchIds(idList);
		
	}
	
	//利用MP进行查询
	@Override
	public EasyUITable findItemByPage(Integer page, Integer rows) {
		
		QueryWrapper<Item> queryWrapper = new QueryWrapper<>();
		queryWrapper.orderByDesc("updated");
		//Page 参数1: page 表示页码值 参数2 : rows 表示页面大小
		IPage<Item> iPage = new Page<>(page, rows);
		//根据分页模型执行分页操作,并将结果返回给分页对象
		iPage = itemMapper.selectPage(iPage, queryWrapper);
		long total = iPage.getTotal();//获取总记录
		List<Item> itemList = iPage.getRecords();//获取当前页记录
		return new EasyUITable(total, itemList);
	}
	//加载商品描述
	@Override
	public ItemDesc findItemDescById(Long itemId) {
			ItemDesc itemDesc = itemDescMapper.selectById(itemId);
		return itemDesc;
	}
	
	
	
	
	
}
