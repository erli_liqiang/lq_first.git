package com.jt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.jt.service.FileService;
import com.jt.vo.ImageVO;

@RestController
public class FileController {
	@Autowired
	private FileService fileService;
	 /**
     * 	实现图片上传操作.
     * 	url地址:http://localhost:8091/pic/upload?dir=image
     * 	参数信息: uploadFile
     * 	返回值: ImageVO对象
     */
	@RequestMapping("/pic/upload")
	public ImageVO upload(MultipartFile uploadFile) {
		return fileService.upload(uploadFile);
	}
	
	
}
