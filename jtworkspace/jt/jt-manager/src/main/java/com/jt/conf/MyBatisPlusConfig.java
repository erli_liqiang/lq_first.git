package com.jt.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
//标志为一个配置类
@Configuration
public class MyBatisPlusConfig {
	//将分页的拦截器交给spring容器管理
	@Bean
	public PaginationInterceptor paginationInterceptor() {
		
		return new PaginationInterceptor();
	}
}
