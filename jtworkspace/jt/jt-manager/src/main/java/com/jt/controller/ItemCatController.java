package com.jt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jt.pojo.ItemCat;
import com.jt.service.ItemCatService;
import com.jt.vo.EasyUITree;

@RestController
@RequestMapping("/item/cat")
public class ItemCatController {
	@Autowired
	private ItemCatService itemCatService;
	
	@RequestMapping("queryItemName")
	public String findItemCatNameById(Long  itemCatId) {
		
		ItemCat itemCat =	itemCatService.findItemCatById(itemCatId);
		return itemCat.getName();
	}
	
	//查询商品分类树形结构
	@RequestMapping("/list")
	public List<EasyUITree> findItemCatNameByParentId(Long id){
		Long parentId = id==null?0l:id;
		return itemCatService.findItemCatNameByParentId(parentId);
		//通过缓存查询商品分类信息
//		return itemCatService.findItemCatNameByCache(parentId);
		
	}
	
}
