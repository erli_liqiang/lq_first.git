package com.jt.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.annotation.CacheFind;
import com.jt.mapper.itemCatMapper;
import com.jt.pojo.ItemCat;
import com.jt.utils.ObjectMapperUtils;
import com.jt.vo.EasyUITree;

import redis.clients.jedis.Jedis;
@Service
public class ItemCatServiceImpl implements ItemCatService {
	@Autowired
	private itemCatMapper itemCatMapper;
	//@Autowired
	//private Jedis jedis;
	//通过redis查询商品分类树
	
//	@Override
//	public List<EasyUITree> findItemCatNameByCache(Long parentId) {
//		
//		List<EasyUITree> treeList = new ArrayList<>();
//		//定义一个key
//		String key = "ITME_CAT_LIST:"+parentId;
//		long start = System.currentTimeMillis();
//		//判断该key是否存在
//		if (jedis.exists(key)) {
//			//存在则从缓存中取数据
//			String json = jedis.get(key);
//			long end = System.currentTimeMillis();
//			treeList = ObjectMapperUtils.toObject(json, treeList.getClass());
//			System.out.println("redis中取数据的时间"+(end-start));
//			
//		}else {
//			//否则从数据库取
//			treeList = findItemCatNameByParentId(parentId);
//			long end = System.currentTimeMillis();
//			//将集合转化为串
//			String json = ObjectMapperUtils.toJSON(treeList);
//			//将取出来的数据存入redis
//			jedis.set(key, json);
//			System.out.println("数据库中取数据的时间"+(end-start));
//		}
//		return treeList;
//	}
	//商品列表中商品分类名称的查询
	@CacheFind(key="ITEM_CAT_NAME")
	@Override
	public ItemCat findItemCatById(Long itemCatId) {
		
		ItemCat itemCat = itemCatMapper.selectById(itemCatId);
		return itemCat;
	}
	//通过数据库查询商品分类树
	@CacheFind(key="ITEM_CAT_LIST")
	@Override
	public List<EasyUITree> findItemCatNameByParentId(Long parentId) {
		
		QueryWrapper<ItemCat> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("parent_id", parentId);
		
		List<ItemCat> itemCatList=itemCatMapper.selectList(queryWrapper);
		
		List<EasyUITree> trees = new ArrayList<>();
		for (ItemCat itemCat : itemCatList) {
			EasyUITree easyUITree = new EasyUITree();
			easyUITree.setId(itemCat.getId());
			easyUITree.setText(itemCat.getName());
			easyUITree.setState(itemCat.getIsParent()?"closed":"open");
			trees.add(easyUITree);
		}
		
		return trees;
	}

}
