package com.jt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.service.ItemService;
import com.jt.vo.EasyUITable;
import com.jt.vo.SysResult;

@RestController
@RequestMapping("/item")
public class ItemController {
	
	@Autowired
	private ItemService itemService;
	/**
	 * 	根据分页数据展现商品的列表数据
	 * 	要求将最新最热门的商品首先给用户展示
	 * 	url以及参数 : /item/query?page=1&rows=20
	 * @param page  当前页
	 * @param rows	记录数
	 * @return	EasyUITable
	 */
	@RequestMapping("/query")
	public EasyUITable doFindEasyUITable(Integer page,Integer rows) {
		//调用业务层对象,进行分页数据的查询
		return itemService.findItemByPage(page,rows);
		
	}
	//保存商品操作
	@RequestMapping("/save")
	public SysResult saveItem(Item item,ItemDesc itemDesc) {
		itemService.saveItem(item,itemDesc);
		return SysResult.success();
	}
	//修改商品
	@RequestMapping("/update")
	public SysResult updateItem(Item item,ItemDesc itemDesc) {
		itemService.updateItem(item,itemDesc);
		return SysResult.success();
	}
	//删除商品
	@RequestMapping("/delete")
	public SysResult deleteItems(Long[] ids) {
		itemService.deleteItems(ids);
		return SysResult.success();
	}
	//商品上架下架操作
	@RequestMapping("{status}")
	public SysResult updownStatus(Long[] ids,@PathVariable("status")String status) {
		if (status.equals("reshelf")) {
			//商品上架
			itemService.updateStatus(ids,1);
			return SysResult.success();
		}else {
			//商品下架
			itemService.updateStatus(ids,2);
			return SysResult.success();
		}
	}
	//加载商品描述 
	/**
	 * 	 业务:根据详情ID查询商品详情信息,之后再页面中回显.
	 * 	url地址:http://localhost:8091/item/query/item/desc/1474391973
	 * 	参数:  包含在url中,利用restFul方式动态获取
	 *	 返回值:  SysResult对象
	 * @return
	 */
	@RequestMapping("/query/item/desc/{itemId}")
	public SysResult findItemDescById(@PathVariable("itemId")Long itemId) {
		ItemDesc itemDesc =itemService.findItemDescById(itemId);
		return SysResult.success(itemDesc);
	}
	
	
}
