package com.jt.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ImageVO implements Serializable{
	private Integer error;//确认是否有误,  0 正常 1 错误
	private String url;
	private Integer width;
	private Integer height;
	
	public static ImageVO fail() {
		return new ImageVO(1,null,null,null);
	}
	public static ImageVO success(String url) {
		return new ImageVO(0, url, null, null);
	}
	public static ImageVO success(String url,Integer width,Integer height) {
		return new ImageVO(0, url, width, height);
	}

}
