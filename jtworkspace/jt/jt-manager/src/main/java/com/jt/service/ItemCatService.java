package com.jt.service;

import java.util.List;

import com.jt.pojo.ItemCat;
import com.jt.vo.EasyUITree;

public interface ItemCatService {

	ItemCat findItemCatById(Long itemCatId);
	//查询商品的分类树
	List<EasyUITree> findItemCatNameByParentId(Long parentId);
	//通过缓存查询商品的分类树
	//List<EasyUITree> findItemCatNameByCache(Long parentId);

}
