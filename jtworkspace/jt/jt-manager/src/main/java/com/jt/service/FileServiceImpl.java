package com.jt.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.jt.vo.ImageVO;
@Service
@PropertySource(value = "classpath:/properties/image.properties")
public class FileServiceImpl implements FileService {
	//定义根目录
	@Value("${image.localDirPath}")
	private  String  localDirPath; //= "D:/JT-SOFT/images";
	//定义虚拟根路径
	@Value("${image.imageUrl}")
	private String imageUrl;
	private static Set<String> imageTypeSet = new HashSet<>();
	static {
		imageTypeSet.add(".jpg");
		imageTypeSet.add(".png");
		imageTypeSet.add(".gif");
		
	}
	
	/**
     * 1.校验文件有效性    .jpg|.png|.gif.......
     * 2.校验文件是否为恶意程序     (木马.exe).jpg
     * 3.提高用户检索图片的效率   分目录存储.
     * 4.为了防止重名图片的提交   自定义文件名称.
     * 5.实现图片的物理上传     本地磁盘中.
     * 6.准备一个访问图片的虚拟路径
     * @param uploadFile
     * @return
     */
	@Override
	public ImageVO upload(MultipartFile uploadFile) {
		//1 校验图片的类型 可以通过集合进行校验
		//获取文件的名称
		String fileName = uploadFile.getOriginalFilename();
		//将文件名称全小写
		fileName = fileName.toLowerCase();
		//获取文件名称的后缀
		String fileType = fileName.substring(fileName.lastIndexOf("."));
		if (!imageTypeSet.contains(fileType)) {
			return ImageVO.fail();
		}
		//2.如何判断文件是否为恶意程序?  文件是否有图片的特有属性!!!!
        //2.1将上传的文件类型利用图片的API进行转化 如果转化不成功则一定不是图片.
		try {
			BufferedImage bufferImage = ImageIO.read(uploadFile.getInputStream());
			int width = bufferImage.getWidth();
			int height = bufferImage.getHeight();
			//根据图片的特有属性进行判断 宽度和高度
			if (width ==0 || height ==0) {
				return ImageVO.fail();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ImageVO.fail();
		}
		//提高用户检索图片的效率   分目录存储.
		//通过时间格式化进行目录分级存储
		String datePath = new SimpleDateFormat("/yyyy/MM/dd/").format(new Date());
		
		//生成文件目录 两部分组成 根目录和时间目录
		String localDir = localDirPath+datePath;
		File dirFile = new File(localDir);
		if (!dirFile.exists()) {
			dirFile.mkdirs();
		}
		//为了防止文件重名发生覆盖,需要自定义名称 UUID
		String uuidName = UUID.randomUUID().toString().replace("-", "");
		//动态生成文件名
		String uuidFileName = uuidName+fileType;
		//实现上传文件路径
		String realFilePath = localDir+uuidFileName;
		//封装文件真实对象
		File imageFile = new File(realFilePath);
		try {
			uploadFile.transferTo(imageFile);
		}catch (IOException e) {
			e.printStackTrace();
			return ImageVO.fail();//告知文件上传失败
		}
		//动态拼接虚拟路径
		String url =imageUrl+datePath+uuidFileName;
		return ImageVO.success(url);
	}

}
