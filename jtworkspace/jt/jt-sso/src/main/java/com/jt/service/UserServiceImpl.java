package com.jt.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;

@Service
public class UserServiceImpl implements UserService {
	
	private static Map<Integer, String> map = new HashMap<>();
	static {
		map.put(1, "username");
		map.put(2, "phone");
		map.put(3, "email");
	}
	
	@Autowired
	private UserMapper userMapper;
	@Override
	public boolean checkUser(String param, Integer type) {
		String column = map.get(type);
		QueryWrapper<User> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq(column, param);
		int count =userMapper.selectCount(queryWrapper);
		return count>0?true:false;
	}
	@Override
	public void saveUser(User user) {
		user.setPhone("12312345678").setEmail("123@qq.com");
		userMapper.insert(user);
		
	}
}
