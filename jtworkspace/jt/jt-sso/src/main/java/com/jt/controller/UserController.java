package com.jt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.jt.pojo.User;
import com.jt.service.UserService;
import com.jt.vo.SysResult;

@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userService;
	@RequestMapping("/getMsg")
	public String getMsg() {
		return "单点登录测试成功";
	}
	/**
     *  1. url请求地址 http://sso.jt.com/user/check/{param}/{type}
     *  2.请求参数 {需要校验的数据}/{校验类型}
     *  3. 返回结果 SysResult返回 需要包含true/false
     *  4. JSONP请求方式,返回值类型必须经过特殊格式的封装 callback(json)
     */
	@RequestMapping("/check/{param}/{type}")
	public JSONPObject checkUser(@PathVariable("param")String param,
			@PathVariable("type")Integer type,
			String callback) {
		boolean flag =userService.checkUser(param,type);
		return new JSONPObject(callback,SysResult.success(flag) );
		
	}
	@RequestMapping("/httpClient/saveUser")
	public SysResult saveUser(User user) {
		userService.saveUser(user);
		return SysResult.success();
	}
	
	
	
	
	
	
	
	
}
