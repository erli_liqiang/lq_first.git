package com.jt.pojo;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

//商品详情pojo对象

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tb_item_desc")
public class ItemDesc extends BasePojo implements Serializable {
	
	
	private static final long serialVersionUID = 6393449377998055042L;
	@TableId
	private Long itemId;
	private String itemDesc;
}
