package com.jt.config;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;

//标识该类是一个配置类,交给spring容器去管理bean对象
@Configuration
@PropertySource(value = "classpath:/properties/redis.properties")
public class RedisConfig {
	@Value("${redis.nodes}")
	private String nodes;
	
	@Bean
	public JedisCluster jedisCluster() {
		
		Set<HostAndPort> set = new HashSet<>();
		
		//获取结点信息
		String[] strNode = nodes.split(",");
		for (String node : strNode) {
			
			String host = node.split(":")[0];
			int port = Integer.parseInt(node.split(":")[1]);
			HostAndPort hostAndPort = new HostAndPort(host, port);
			set.add(hostAndPort);		
		}
		return new JedisCluster(set);	
		
	}
	
	
	
	
	
//	@Value("${redis.host}")
//	private String host;
//	@Value("${redis.port}")
//	private Integer port;
//	
//	@Bean
//	public Jedis jedis() {
//		return new Jedis(host,port);
//	}

}
