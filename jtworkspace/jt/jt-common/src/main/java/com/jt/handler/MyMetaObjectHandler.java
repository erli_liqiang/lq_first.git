package com.jt.handler;

import java.util.Date;

import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

	@Override
	public void insertFill(MetaObject metaObject) {
		this.setInsertFieldValByName("update", new Date(), metaObject);
		this.setInsertFieldValByName("created", new Date(), metaObject);
		
	}

	@Override
	public void updateFill(MetaObject metaObject) {
		this.setUpdateFieldValByName("updated", new Date(), metaObject);
			
	}

}
