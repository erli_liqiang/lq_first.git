package com.jt.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jt.annotation.CacheFind;
import com.jt.utils.ObjectMapperUtils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;

@Component
@Aspect
public class CacheAOP {
	@Autowired
//	private Jedis jedis;
	private JedisCluster jedis;
	
	
	@Around("@annotation(cacheFind)")
	public Object around(ProceedingJoinPoint joinPoint,CacheFind cacheFind) {

		//获取注解的参数的前缀
		String key = cacheFind.key();
		//动态获取方法中的第一个参数
		String firstArg = joinPoint.getArgs()[0].toString();
		key+=":"+firstArg;

		Object result =null;
		//判断key是否存在于redis中
		if (jedis.exists(key)) {
			//存在的话则直接获取
			String json = jedis.get(key);
			//获取返回值类型
			MethodSignature methodSignature =(MethodSignature)  joinPoint.getSignature();
			Class<?> returnType = methodSignature.getReturnType();
			result = ObjectMapperUtils.toObject(json, returnType);
			System.out.println("aop查询redis缓存");
		}else {
			//不存在则从数据库中查询,然后放入redis中
			 try {
				result = joinPoint.proceed();
				System.out.println("AOP查询数据库获取返回值结果");
				
				//将 查询的结果转化为json串存入redis
				String json = ObjectMapperUtils.toJSON(result);
				
				int seconds = cacheFind.seconds();
				if (seconds>0) {
					jedis.setex(key, seconds, json);
				}else {
					jedis.set(key, json);
				}
				
			} catch (Throwable e) {
				e.printStackTrace();
			}
			
		}
		return result;
	}

}
