package com.jt.aop;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.jt.vo.SysResult;
//表明该类是一个控制层异常处理类
@RestControllerAdvice
public class SystemExceptionAOP {
	@ExceptionHandler(RuntimeException.class)
	public Object fail(Throwable throwable,HttpServletRequest request) {
		
		String callback = request.getParameter("callback");
		if (StringUtils.isEmpty(callback)) {
			
			//打印异常信息
			throwable.printStackTrace();
			return SysResult.fail();
			
		}else {
			throwable.printStackTrace();
			return new JSONPObject(callback, SysResult.fail());
			
		}
		
		
		
	}
	
}
