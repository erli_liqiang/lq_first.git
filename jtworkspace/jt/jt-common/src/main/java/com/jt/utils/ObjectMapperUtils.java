package com.jt.utils;

import javax.management.RuntimeErrorException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectMapperUtils {
	
	//定义一个API对象
	private static final ObjectMapper MAPPER = new ObjectMapper();
	//封装API,将对象转化为json
	
	public static String toJSON(Object object) {
		
		if (object==null) {
			throw new RuntimeException("传入对象不能为null");
		}
		try {
			return MAPPER.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			throw new RuntimeException("传入对象不能转化");
		}
		
	}
	
	//封装API,将json转化为对象
	public static <T> T toObject(String json,Class<T> target) {
		if (json==null || "".equals(json) || target==null) {
			throw new RuntimeException("传入的字符串不能为null");
		}
		T t;
		try {
			t = MAPPER.readValue(json, target);
			return t;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("不能将该串转化为对象");
		} 
		
		
	}
	

}
