package m2_work;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

//工作模式下的生产者
public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {
        //连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("www.gavin6.xyz");
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        Channel c = f.newConnection().createChannel();

        //定义队列
        c.queueDeclare("task_queue",
                true,
                false,
                false,
                null);
        //发送消息
        while (true){
            System.out.println("输入消息");
            String string =new Scanner(System.in).nextLine();
            //如果输入的是"exit"则结束生产者进程
            if ("exit".equals(string)) {
                break;
            }
            c.basicPublish("","task_queue", MessageProperties.PERSISTENT_TEXT_PLAIN,string.getBytes());
            System.out.println("消息已经发送");
        }
        c.close();


    }




}
