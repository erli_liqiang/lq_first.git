package m2_work;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consumer {
    public static void main(String[] args) throws IOException, TimeoutException {
        //连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("www.gavin6.xyz");
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        Channel c = f.newConnection().createChannel();
        //定义队列

        c.queueDeclare("task_queue",
                true, //持久化队列
                false,
                false,
                null);


        DeliverCallback deliverCallback = new DeliverCallback() {
            @Override
            public void handle(String consumerTag, Delivery message) throws IOException {
                byte[] body = message.getBody();
                String msg = new String(body);
                System.out.println("收到:"+msg);
                for (int i = 0; i <msg.length() ; i++) {
                    //遍历到 . 就休息一秒
                    if ('.' == msg.charAt(i)){
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                //发送回执
                //参数解释 参数1 回执 参数2 是否确认多条消息回执
                c.basicAck(message.getEnvelope().getDeliveryTag(),false);
                System.out.println("处理完毕结束-----------");

            }
        };
        CancelCallback cancelCallback = new CancelCallback() {
            @Override
            public void handle(String consumerTag) throws IOException {

            }
        };

        //设置每次抓取的数量 --每次只抓取一条数据
        c.basicQos(1);

        //消费数据
        //手动ack设置 将true改为false
        c.basicConsume("task_queue",false,deliverCallback,cancelCallback);


    }
}
