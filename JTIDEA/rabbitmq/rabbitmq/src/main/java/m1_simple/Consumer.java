package m1_simple;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

//消费者
public class Consumer {

    public static void main(String[] args) throws IOException, TimeoutException {
        //1.创建连接
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.168.64.140");//www.gavin6.xyz
        factory.setPort(5672); //通信端口
        factory.setUsername("admin");
        factory.setPassword("admin");
//        factory.setVirtualHost("/lq");//使用虚拟主机需要设置
        //打开通道
        Channel c = factory.newConnection().createChannel();
        //2.定义队列
        //告诉服务器想使用该队列
        c.queueDeclare("helloworld",false,false,false,null);

        //收到消息后用来处理消息的回调对象
//        DeliverCallback callback = new DeliverCallback() {
//            @Override
//            public void handle(String consumerTag, Delivery message) throws IOException {
//                String msg = new String(message.getBody(), "UTF-8");
//                System.out.println("收到: "+msg);
//            }
//        };
        DeliverCallback deliverCallback = new DeliverCallback() {
            @Override
            public void handle(String s, Delivery delivery) throws IOException {
                byte[] str = delivery.getBody();
                String msg = new String(str);
                System.out.println("收到:" + msg);
            }
        };

        //消费者取消时的回调对象
        CancelCallback cancel = new CancelCallback() {
            @Override
            public void handle(String consumerTag) throws IOException {
            }
        };
        //3.消费数据
        c.basicConsume("helloworld",true,deliverCallback,cancel);

    }
}
