package m1_simple;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

//生产者模式
public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {
        //1.创建连接
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.168.64.140");//www.gavin6.xyz 192.168.64.140
        factory.setPort(5672); //通信端口
        factory.setUsername("admin");
        factory.setPassword("admin");
//        factory.setVirtualHost("/lq");//使用虚拟主机需要设置
        //打开通道
        Channel c = factory.newConnection().createChannel();
        //2.定义队列
        //指定用来发送消息的队列,如果队列不存在,服务器则会创建
        //如果存在,则直接使用该队列
        c.queueDeclare("helloworld",
                false,
                false,
                false,
                null);

        //3.发送消息
        c.basicPublish("","helloworld",null,"helloworld!".getBytes());
        System.out.println("消息已经发送");
        //断开连接
        c.close();
    }




}
