package m5_topic;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

//路由模式下的消费者
public class Consumer {
    public static void main(String[] args) throws IOException, TimeoutException {

        //连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("www.gavin6.xyz");
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        Channel c = f.newConnection().createChannel();
        //定义交换机
        c.exchangeDeclare("topic_logs", BuiltinExchangeType.TOPIC);
        //定义随机队列

        String queue = UUID.randomUUID().toString();
        c.queueDeclare(queue,false,true,true,null);

        System.out.println("输入绑定键,用空格隔开");
        String keys = new Scanner(System.in).nextLine();
        String[] a = keys.split("\\s+");
        //用绑定键用来绑定
        for (String key : a) {
            c.queueBind(queue,"topic_logs",key);
        }

        DeliverCallback deliverCallback = new DeliverCallback() {
            @Override
            public void handle(String consumerTag, Delivery message) throws IOException {
                byte[] body = message.getBody();
                String msg = new String(body);
                Envelope envelope = message.getEnvelope();
                String key = envelope.getRoutingKey();
                System.out.println("收到消息"+msg+",key="+key);
            }
        };
        CancelCallback cancelCallback = new CancelCallback() {
            @Override
            public void handle(String consumerTag) throws IOException {

            }
        };

        //消费数据
        c.basicConsume(queue,true, deliverCallback, cancelCallback);


    }
}
