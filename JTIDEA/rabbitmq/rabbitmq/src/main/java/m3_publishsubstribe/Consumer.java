package m3_publishsubstribe;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

//消费者
public class Consumer {

    public static void main(String[] args) throws IOException, TimeoutException {
        //连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("www.gavin6.xyz");
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        Channel c = f.newConnection().createChannel();
        //定义交换机
        c.exchangeDeclare("logs", BuiltinExchangeType.FANOUT);

        //定义随机队列
        //方式一 自己给出所有参数
//        String queue = UUID.randomUUID().toString();
//        c.queueDeclare(queue,false,true,true,null);
        //方式二 服务器自动命名,并获取队列名字
        String queue = c.queueDeclare().getQueue();
        //绑定
        c.queueBind(queue,"logs","");

        DeliverCallback deliverCallback = new DeliverCallback() {
            @Override
            public void handle(String consumerTag, Delivery message) throws IOException {
                byte[] body = message.getBody();
                String msg = new String(body);
                System.out.println("收到消息"+msg);
            }
        };
        CancelCallback cancelCallback = new CancelCallback() {
            @Override
            public void handle(String consumerTag) throws IOException {

            }
        };

        //消费数据
        c.basicConsume(queue,true, deliverCallback, cancelCallback);

    }
}
