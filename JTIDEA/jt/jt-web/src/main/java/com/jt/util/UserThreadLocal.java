package com.jt.util;

import com.jt.pojo.User;

public class UserThreadLocal {

    //定义本地线程变量
    private static  ThreadLocal<User> threadLocal = new ThreadLocal<>();
    //定义数据新增的方法

    public   static  void set(User user){
        threadLocal.set(user);
    }
    //获取数据的方法

    public  static  User get(){
        return  threadLocal.get();
    }
    //移除方法 ,使用threadlocal时切记将数据移除,否则极端条件下,容易产生内存泄露
    public static  void remove(){
        threadLocal.remove();
    }

}
