package com.jt.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.jt.pojo.User;
import com.jt.service.DubboUserService;
import com.jt.utils.CookieUtils;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import redis.clients.jedis.JedisCluster;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/user")
public class UserController {
    @Reference(check = false) //表示消费者启动时,检测是否有服务的提供者,false表示不检查,调用时校验
    private DubboUserService dubboUserService;
    //注入redis集群
    @Autowired
    private JedisCluster jedisCluster;

    /**
     * 业务说明 : 要求用一个方法实现页面的通用跳转
     * restful分格
     */
    @RequestMapping("/{moduleName}")
    public String toPage(@PathVariable("moduleName") String moduleName){
//        System.out.println("moudleName"+moduleName);
        return moduleName;
    }

    /**
     *  url地址 http://www.jt.com/user/doRegister
     *  返回值 sysResult
      */
    @RequestMapping("/doRegister")
    @ResponseBody
    public SysResult diRegister(User user){
        //第三方接口 直接rpc调用访问sso中的实现类
        dubboUserService.saveUser(user);
        return  SysResult.success();
    }

    /**
     * 請求地址: http://www.jt.com/user/doLogin?r=0.4026067653434138
     * 參數 username password
     * 返回值結果 SysResult
     * 需求: 將cookie名称为JT_TICKET数据输出到浏览器中,要求7天超时,并且实现"jt.com"数据共享
     */
    @RequestMapping("/doLogin")
    @ResponseBody
    public  SysResult doLogin(User user, HttpServletResponse response){
        //完成用户登录操作,然后需要获取ticket密钥
        String ticket = dubboUserService.doLogin(user);
        if (StringUtils.isEmpty(ticket)){
            //说明用户名或者密码有问题
            return  SysResult.fail();
        }
        //创建cookie对象
        Cookie cookie = new Cookie("JT_TICKET",ticket);
        //设置cookie的存活时间 -1 表示关闭当前会话,cookie删除
        //设置cookie的存活时间 0  表示立即cookie删除
        //设置cookie的存活时间 >0  表示设置cookie的超时时间
        cookie.setMaxAge(7*24*60*60);
        cookie.setPath("/");
        //在jt.com中实现数据的共享
        cookie.setDomain("jt.com");
        //添加cookie,将数据保存到浏览器
        response.addCookie(cookie);



        return  SysResult.success();
    }

    /**
     * 用户退出操作
     * 请求的url:http://www.jt.com/user/logout.html
     * 退出之后,重定向到首页页面
     */
    private static final   String  TICKET="JT_TICKET";
    @RequestMapping("/logout")
    public String doLogout(HttpServletRequest request,HttpServletResponse response){

        //删除cookie信息 设置生存时间为0
        //获取cookie信息
        Cookie cookie = CookieUtils.getCookieByName(request, TICKET);
        if (cookie!=null){
            String jtTicket = cookie.getValue();
            //删除redis信息
            if (!StringUtils.isEmpty(jtTicket)){
                //如果数据不为null,则开始执行退出操作.
                jedisCluster.del(jtTicket);//根据key,删除Redis中的记录
            //删除cookie
            CookieUtils.deleteCookie(TICKET,"/","jt.com",response);
            }
        }
        //重定向到首页
        return  "redirect:/";
    }

//    @RequestMapping("/logout")
//    public String doLogout(HttpServletRequest request,HttpServletResponse response){
//        String  jtTicket = null;
//        //删除cookie信息 设置生存时间为0
//        //获取cookie信息
//        Cookie[] cookies = request.getCookies();
//        //校验cookie是否为空
//        if (cookies!=null && cookies.length>0) {
//            for (Cookie cookie : cookies) {
//                //获取cookie的值,为了删除redis中数据
//                if ("JT_TICKET".equalsIgnoreCase(cookie.getName())){
//                    jtTicket= cookie.getValue();
//                    cookie.setMaxAge(0);
//                    cookie.setDomain("jt.com");
//                    cookie.setPath("/");
//                    response.addCookie(cookie);
//                    break;
//                }
//
//            }
//        }
//        //删除redis信息
//        if (!StringUtils.isEmpty(jtTicket))
//            //如果数据不为null,则开始执行退出操作.
//            jedisCluster.del(jtTicket);//根据key,删除Redis中的记录
//
//        //重定向到首页
//        return  "redirect:/";
//    }



}
