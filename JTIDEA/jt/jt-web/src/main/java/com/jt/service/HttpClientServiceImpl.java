package com.jt.service;

import com.jt.pojo.User;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class HttpClientServiceImpl implements  HttpClientService{
    /**
     * http://sso.jt.com/user/httpClient/saveUser?
     * @param user
     */
    @Override
    public void saveUser(User user) {

        String url ="http://sso.jt.com/user/httpClient/saveUser?username="
                +user.getUsername()+"&password="+user.getPassword();
        CloseableHttpClient httpClient = HttpClients.createDefault();
        //请求方式
        HttpGet get = new HttpGet(url);
        //发起请求
        try {
            CloseableHttpResponse httpResponse = httpClient.execute(get);
            if (httpResponse.getStatusLine().getStatusCode()!=200){
                throw  new RuntimeException("请求错误");
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("请求错误");
        }
    }
}
