package com.jt.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jt.pojo.Cart;
import com.jt.pojo.Order;
import com.jt.pojo.User;
import com.jt.service.DubboCartService;
import com.jt.service.DubboOrderService;
import com.jt.util.UserThreadLocal;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/order")
public class OrderController {
    @Reference(check = false)
    private DubboCartService dubboCartService;
    @Reference(check = false)
    private DubboOrderService dubboOrderService;


    /**
     * 订单页面跳转
     * http://www.jt.com/order/create.html
     */
    @RequestMapping("/create")
    public String createOrder(HttpServletRequest request, Model model){
        User user =(User) request.getAttribute("JT_USER");
        Long userId = user.getId();
        List<Cart> cartList = dubboCartService.findCartListByUserId(userId);
        model.addAttribute("carts",cartList);
        return "order-cart";
    }

   /**
    * 1.实现订单入库
    *   url:http://www. jt. com/order/submit
    *   参数:整个form表单利用order对象 接收
    *   返回值: SysResult对象 返回orderId
    *   业务:订单入库时应该入库3张表记录. order orderShipping orderItems
    *   orderId由登录用户id+当前时间戳手动拼接.
    *   并且要求三个对象的主键值相同.
    */
   @RequestMapping("/submit")
   @ResponseBody
   public SysResult submit(Order order,HttpServletRequest request){
        //获取用户的id
       User user = (User) request.getAttribute("JT_USER");
       Long userId = user.getId();
       order.setUserId(userId);	//将userId进行赋值操作.
       //订单入库操作
       String orderId =dubboOrderService.saveOrder(order);
       if (StringUtils.isEmpty(orderId)){
           return  SysResult.fail();
       }
       return  SysResult.success(orderId);

   }

    /**
     *  实现订单的查询 更具orderId
     *  url请求地址http://www.jt.com/order/success.html?id=71598258270221
     *  参数是 id
     *  返回值 success页面\
     *  页面参数 ${order.orderId}
     */
    @RequestMapping("/success")
    public String  findOrderById(String id,Model model){
        Order order = dubboOrderService.findOrderById(id);
        model.addAttribute("order",order);

        return "success";
    }



}
