package com.jt.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jt.pojo.Cart;
import com.jt.pojo.User;
import com.jt.service.DubboCartService;
import com.jt.util.UserThreadLocal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

//由于需要使用返回页面逻辑的名称所以使用以下注解
@Controller
@RequestMapping("/cart")
public class CartController {
    @Reference(check = false)
    private DubboCartService dubboCartService;

    /**
     * 1.购物车列表数据展现
     * url: http://www.jt.com/cart/show.html
     * 参数: 无
     * 返回值: cart  页面逻辑名称
     * 页面取值数据: ${cartList}
     * 核心业务流程: 根据userId查询购物车记录.  userId = 7L;
     */
    @RequestMapping("/show")
    public String cartList(Model model,HttpServletRequest request){

        User user =(User) request.getAttribute("JT_USER");
        Long userId =user.getId();
        List<Cart> cartList =dubboCartService.findCartListByUserId(userId);
        model.addAttribute("cartList",cartList);
        return  "cart";
    }

    /**
     * 修改购物车商品数量
     * url请求地址:http://www.jt.com/cart/update/num/562379/8
     *  参数 itemId/num
     *  返回值 void
     *  条件 userId =7l;
     */
    @RequestMapping("/update/num/{itemId}/{itemNum}")
    @ResponseBody
    //public void updateCartNum(@PathVariable("itemId") Long itemId,@PathVariable("itemNum") Integer itemNum){
    public void updateCartNum(Cart cart,HttpServletRequest request){//restFul中的参数与对象的属性名称一致,可以简化
        User user =(User) request.getAttribute("JT_USER");
        //获取用户id
        Long userId = user.getId();

        cart.setUserId(userId);
        dubboCartService.updateCatNum(cart);

    }
    /**
     * 删除购物车
     * 1.url地址:http://www.jt.com/cart/delete/562379.html
     * 2.参数:itemId
     * 3.返回值: 重定向到购物车列表页面
     */
    @RequestMapping("/delete/{itemId}")
    public String deleteCart(Cart cart){
        //动态获取userId;
        Long userId = UserThreadLocal.get().getId();
        cart.setUserId(userId);
        //执行删除业务
        dubboCartService.deleteCart(cart);
        return  "redirect:/cart/show.html";
    }

    /**
    * 业务需求: 完成购物车新增
    * 1.url地址:http:/ /www. jt. com/cart/add/562379. htmL
    * 2.参数:整 合cart的form表单
    * 3. 返回值:新增购物车完成之后,应该重定向到购物车展现页面
    * 注意事项:如果用户重复添加购物车则只修改数量即可.
    */

    @RequestMapping("/add/{itemId}")
    public String addCart(Cart cart , HttpServletRequest request){
        //新增购物车的时候,需要根据用户的id进行
        User user =(User) request.getAttribute("JT_USER");

        Long userId = user.getId();
        cart.setUserId(userId);
        dubboCartService.addCart(cart);
        return "redirect:/cart/show.html";

    }

}
