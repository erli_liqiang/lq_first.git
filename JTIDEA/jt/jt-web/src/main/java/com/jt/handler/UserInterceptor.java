package com.jt.handler;

import com.jt.pojo.User;
import com.jt.util.UserThreadLocal;
import com.jt.utils.CookieUtils;
import com.jt.utils.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import redis.clients.jedis.JedisCluster;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//拦截器的类  拦截器的配置文件(拦截什么请求)
@Component //将拦截器交给容器管理
public class UserInterceptor  implements HandlerInterceptor {
    @Autowired
    private JedisCluster jedisCluster;
    /**
     *  实现pre的方法
     *  返回值说明:
     *          return  false 表示拦截 需要配合重定向一齐使用
     *          return  ture  表示放行
     *  需求1: 如果用户没有登录,则重定向到系统登录页面
     *  判断条件: 如何判断用户是否登录.  1.检查Cookie中是否有记录   2.Redis中是否有记录.
     */

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        Cookie jtTicket = CookieUtils.getCookieByName(request, "JT_TICKET");
        if (jtTicket!=null){
            String ticket =jtTicket.getValue();

            if (jedisCluster.exists(ticket)){
                //如果redis中的数据存在,则可能已经登录,可以放行
                String userJSON = jedisCluster.get(ticket);
                User user = ObjectMapperUtils.toObject(userJSON, User.class);
                request.setAttribute("JT_USER",user);
                //利用ThreadLocal方式存储数据
                UserThreadLocal.set(user);

                return  true;
            }else {
                //cookie中的记录与redis中的记录不一致,应该删除cookie中的数据
                CookieUtils.deleteCookie("JT_TICKET","/","jt.com",response);
            }

        }

        response.sendRedirect("/user/login.html");

        return false;
    }
    //实现数据的移除
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        UserThreadLocal.remove();
    }
}
