package com.jt;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class TestHttpClient {

    /**
     * 业务需求:在java代码中访问百度页面
     */
    @Test
    public void test01()  {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        String url = "http://news.sohu.com";
        HttpGet get = new HttpGet(url);
        try {
            CloseableHttpResponse response = httpClient.execute(get);
            //获取返回值状态
            int status = response.getStatusLine().getStatusCode();
            if (status==200){
                //获取响应结果
                HttpEntity entity = response.getEntity();//获取响应对象的实体信息
                //将实体对象转化为用户可以识别的字符串
                String result = EntityUtils.toString(entity,"UTF-8");
                System.out.println(result);

            }else {
                System.out.println("httpClient调用异常");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
