package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.User;
import org.apache.ibatis.annotations.Select;

public interface UserMapper extends BaseMapper<User> {
    //@Select("select count(*) from tb_user where username =#{param}")
   // int selectUserByUserName(String param);
}
