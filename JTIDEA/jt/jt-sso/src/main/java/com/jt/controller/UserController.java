package com.jt.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.jt.pojo.User;
import com.jt.service.UserService;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisCluster;

//服务端要求返回的数据都是json
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private JedisCluster jedisCluster;

    @RequestMapping("/getMsg")
    public String getMsg(){
        return  "单点登录测试";
    }

    /**
     *  1. url请求地址 http://sso.jt.com/user/check/{param}/{type}
     *  2.请求参数 {需要校验的数据}/{校验类型}
     *  3. 返回结果 SysResult返回 需要包含true/false
     *  4. JSONP请求方式,返回值类型必须经过特殊格式的封装 callback(json)
     */
    @RequestMapping("/check/{param}/{type}")
    public JSONPObject checkUser(@PathVariable("param") String param , @PathVariable("type") Integer type, String callback) {
        boolean flag = userService.checkUser(param,type);//存在true  不存在false
       // int a =1/0;
        return  new JSONPObject(callback, SysResult.success(flag));
    }
    /**
     * 完成httpClient测试
     * url:http://sso.jt.com/user/httpClient/saveUser?username=111&password="2222"
     */
    @RequestMapping("/httpClient/saveUser")
    public SysResult saveUser(User user){
        userService.saveUser(user);
        return  SysResult.success();

    }

    /**
     * 登录成功以后进行数据回显
     * url地址:http://sso.jt.com/user/query/b11facd89dec4789907e638dfbbd8bf7?callback=jsonp1598089308095&_=1598
     * 参数 ticket
     * 返回值 SysResult 用jsonp封装
     */
    @RequestMapping("/query/{ticket}")
    public JSONPObject doTicket(@PathVariable("ticket") String ticket,String callback){
        String userJson = jedisCluster.get(ticket);
        if (userJson==null){
            return  new JSONPObject(callback,SysResult.fail());
        }
        else {
            return  new JSONPObject(callback,SysResult.success(userJson));
        }

    }






}
