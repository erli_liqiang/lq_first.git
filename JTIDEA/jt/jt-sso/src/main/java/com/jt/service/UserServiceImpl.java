package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserServiceImpl implements  UserService{
    private String column = null;
    //采用工具API形式动态获取
    private static Map<Integer,String> typeMap = new HashMap<>();
    static {//类加载的时候就开始执行,只执行一次
        typeMap.put(1,"username");
        typeMap.put(2,"phone");
        typeMap.put(3,"email");


    }

    @Autowired
    private UserMapper userMapper;
    //保存用户操作
    @Override
    public void saveUser(User user) {
        user.setPhone("13582837925").setEmail("1435430561@qq.com");
        userMapper.insert(user);

    }

    //数据校验
    @Override
    public boolean checkUser(String param, Integer type) {
        column = typeMap.get(type);
//        String column = type==1?"username":(type==2?"phone":"email");
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.eq(column,param);
        int count = userMapper.selectCount(queryWrapper);
        return  count>0? true:false; //如果返回true,表示已经存在,如果返回false,表示可以使用


    }
}
