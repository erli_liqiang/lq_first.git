package com.jt.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import com.jt.utils.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import redis.clients.jedis.JedisCluster;

import java.util.UUID;

@Service //注解是dubbo的包
public class DubboUserServiceImpl implements  DubboUserService {
    @Autowired
    private UserMapper userMapper;

    //redis集群注入
    @Autowired
    private JedisCluster jedisCluster;

    @Override
    public void saveUser(User user) {
        //MD5加密
        String md5Password = DigestUtils.md5DigestAsHex(user.getPassword().getBytes());
        user.setPassword(md5Password);
        String email=user.getPhone()+"@qq.com";
        user.setEmail(email);
        userMapper.insert(user);
    }

    /**
     * 目的 : 校验用户信息是否有效并且实现单点登录操作
     *   步骤
     *   1. 校验用户名和密码是否正确(密码明文转化为密文)
     *   2. 查询数据库是否有结果
     *   3.如果有结果则生成ticket信息(UUID) ,将user转化为userJson
     *   4.将数据保存到redis中,并且设定超时时间
     *   5.返回当前用户登录的密钥
     * @param user
     * @return
     */

    @Override
    public String doLogin(User user) {
        //将明文转化为密文
        String password = user.getPassword();
        String digestAsHex = DigestUtils.md5DigestAsHex(password.getBytes());

        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username",user.getUsername()).eq("password",digestAsHex);
        //获取的是用户的全部记录 (包含涉密信息)
        User userDB =userMapper.selectOne(queryWrapper);
        //检验数据是否有效
        if (userDB==null){
            //如果数据无效,怎返回null
            return  null;
        }
        //如果程序执行到这里,说明用户名和密码正确的. 开启单点登录流程
        String ticket = UUID.randomUUID().toString().replace("-","");
        //脱敏处理 余额/密码/手机号/家庭地址
        userDB.setPassword("123456?你信吗");
        String json = ObjectMapperUtils.toJSON(userDB);
        jedisCluster.setex(ticket,7*24*60*60,json);


        return ticket;
    }
}
