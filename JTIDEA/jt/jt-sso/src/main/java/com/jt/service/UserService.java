package com.jt.service;

import com.jt.pojo.User;

public interface UserService {

    //校验数据
    boolean checkUser(String param, Integer type);

    void saveUser(User user);
}
