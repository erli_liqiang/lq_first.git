package com.jt.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.OrderItemMapper;
import com.jt.mapper.OrderMapper;
import com.jt.mapper.OrderShippingMapper;
import com.jt.pojo.Order;
import com.jt.pojo.OrderItem;
import com.jt.pojo.OrderShipping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OrderServiceImpl implements  DubboOrderService{
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderItemMapper orderItemMapper;
    @Autowired
    private OrderShippingMapper orderShippingMapper;

    @Transactional //事务控制
    @Override
    public String saveOrder(Order order) {
        //orderId 由登录用户id和时间戳构成
        String orderId = ""+order.getUserId()+System.currentTimeMillis();
        //完成订单的入库操作
        order.setOrderId(orderId).setStatus(1);
        orderMapper.insert(order);

        //完成订单商品入库
        List<OrderItem> orderItems = order.getOrderItems();
//        for (OrderItem orderItem : orderItems) {
//            orderItem.setOrderId(orderId);
//            orderItemMapper.insert(orderItem);
//
//        }
       for (OrderItem orderItem : orderItems) {
            orderItem.setOrderId(orderId);
        }
        System.out.println(orderItems);
        orderItemMapper.insertOrderItem(orderItems);
        System.out.println("订单商品入库成功");

        //完成订单物流入库
        OrderShipping orderShipping = order.getOrderShipping();
        orderShipping.setOrderId(orderId);
        orderShippingMapper.insert(orderShipping);
        System.out.println("订单物流入库成功");
        return orderId;
    }
    //查询订单
    @Override
    public Order findOrderById(String id) {
        //查询订单信息
        Order order = orderMapper.selectById(id);
        //查询订单商品
        QueryWrapper<OrderItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("item_id",id);
        List<OrderItem> orderItems = orderItemMapper.selectList(queryWrapper);
        //查询订单物流
        OrderShipping orderShipping = orderShippingMapper.selectById(id);
        //封装order
        order.setOrderItems(orderItems).setOrderShipping(orderShipping);
        return order;
    }
}
