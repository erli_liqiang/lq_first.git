package com.jt.web.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.jt.pojo.ItemDesc;
import com.jt.utils.ObjectMapperUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JSONPController {
    //使用api优化
    @RequestMapping("/web/testJSONP")
    public JSONPObject jsonpObject(String callback){
        ItemDesc itemDesc = new ItemDesc();
        itemDesc.setItemId(100L).setItemDesc("我是商品详情");
        return  new JSONPObject(callback,itemDesc);
    }


//    public  String jsonp(String callback){
//        ItemDesc itemDesc = new ItemDesc();
//        itemDesc.setItemId(100L).setItemDesc("我是商品详情");
//        String json = ObjectMapperUtils.toJSON(itemDesc);
//
//        return  callback+"("+json+")";
//
//    }


}
