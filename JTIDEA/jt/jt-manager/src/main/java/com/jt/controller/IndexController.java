package com.jt.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
	//通用页面跳转
	@RequestMapping("/page/{moduleName}")
	public String doPageUI(@PathVariable("moduleName")String moduleName) {
		return moduleName;
	}
	
}
