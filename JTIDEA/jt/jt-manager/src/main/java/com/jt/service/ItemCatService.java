package com.jt.service;

import com.jt.pojo.ItemCat;
import com.jt.vo.EasyUITree;

import java.util.List;

public interface ItemCatService {

	ItemCat findItemCatById(Long itemCatId);

    List<EasyUITree> findEasyUITree(Long parendId);
    //通过缓存查找商品分类信息
    List<EasyUITree> findItemCatByCache(Long parentId);
}
