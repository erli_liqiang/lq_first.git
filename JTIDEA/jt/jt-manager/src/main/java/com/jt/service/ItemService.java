package com.jt.service;

import java.util.List;

import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.vo.EasyUITable;

public interface ItemService {

	EasyUITable findItemByPage(Integer page, Integer rows);
    //保存商品操作
    void saveItem(Item item , ItemDesc itemDesc);
    //修改商品操作
    void updateItem(Item item  , ItemDesc itemDesc);
    //删除商品
    void deleteItemsByIds(Long[] ids);

    //商品上下架操作
    void updatStatus(Long[] ids, Integer status);

    ItemDesc findItemDescById(Long itemId);
}
