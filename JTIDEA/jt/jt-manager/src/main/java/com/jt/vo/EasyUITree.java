package com.jt.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class EasyUITree implements Serializable {
    private static final long serialVersionUID = -6430564299899005614L;
    //如果涉及到网络传输对象的时候,就需要序列化和反序列化
    private Long id;//根据数据库数据类型为long型
    private String text; //定义结点名称
    private String state;//定义结点开关


}
