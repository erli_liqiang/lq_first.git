package com.jt.controller;

import com.jt.service.FileService;
import com.jt.vo.ImageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@RestController
public class FileController {
    @Autowired
    private FileService fileService;

    /**
     * 完成文件上传的入门案例
     * http://localhost:8091/file
     * 请求参数 : fileImage
     * 返回值: 文件上传成功
     */
    @RequestMapping("/file")
    public String upLoadTest(MultipartFile fileImage)  {
        //设置文件上传目录
        String fileDirPath = "D:/JT-SOFT/images";
        File dirFile = new File(fileDirPath);
        //如果目录不存在,则创建
        if (!dirFile.exists()){
            dirFile.mkdirs();
        }
        //获取文件的名称
        String fileName = fileImage.getOriginalFilename();
        //创建文件上传全路径
        File realPath = new File(fileDirPath+"/"+fileName);
        try {
            fileImage.transferTo(realPath);
            return  "文件上传成功";
        } catch (IOException e) {
            e.printStackTrace();
            return  "文件上传失败";
        }
    }

    /**
     *实现图片上传操作. .
     * url 地址: http://Localhost:8091/pic/upload?dir=image
     *参数信息: uploadFile
     *返回值: ImageVO对象
     */
    @RequestMapping("/pic/upload")
    public ImageVO upload(MultipartFile uploadFile){

        return fileService.upload(uploadFile);
    }



}
