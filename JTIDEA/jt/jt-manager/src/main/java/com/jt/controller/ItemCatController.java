package com.jt.controller;

import com.jt.vo.EasyUITree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jt.pojo.ItemCat;
import com.jt.service.ItemCatService;

import java.util.List;

@RestController
@RequestMapping("/item/cat")
public class ItemCatController {

	@Autowired
	private ItemCatService itemCatService;
	/**
	 * 	url:"/item/cat/queryItemName", //按照业务需要自己定义
    	请求参数:{itemCatId:val}, //请求参数 k-v结构
    	返回值结果 返回商品分类名称
	 * 	基于商品分类id查询商品分类名称
	 * @param itemCatId
	 * @return
	 */
	@RequestMapping("/queryItemName")
	public String findItemCatNameById(Long itemCatId) {
		ItemCat itemCat=itemCatService.findItemCatById(itemCatId);
	return itemCat.getName();
	}
	//实现异步树的加载 id :xxxx

//	@RequestMapping("/list")
//	public List<EasyUITree> findEasyUITree(@RequestParam(value = "id",defaultValue = "0") Long parentId){
//		Long parendId =0L;//根据parentId=0,查询一级商品分类的信息
//		List<EasyUITree> trees =itemCatService.findEasyUITree( parentId);
//		//System.out.println(trees);
//
//		return  trees;
//
//	}

    /**
     *  实现异步树加载: id: xxxx
     * 说明: 当展开一个封闭的节点,才会发起id的参数请求.前提条件是树必须先初始化.
     * 也就是说应该先展现一级商品分类信息.完成树的初始化
     * 判断依据: id是否为null.如果为null则表示第一 次查询需要初始化ID 查询一 级商品分类目录
     * @param id
     * @return
     */
    @RequestMapping("/list")
    public List<EasyUITree> findEasyUITree(Long id){
		Long parentId =id==null?0l:id;//根据parentId=0,查询一级商品分类的信息
        List<EasyUITree> trees =itemCatService.findEasyUITree( parentId);
//		List<EasyUITree> trees =itemCatService.findItemCatByCache(parentId);//手写缓存
        return  trees;

    }
	
}
