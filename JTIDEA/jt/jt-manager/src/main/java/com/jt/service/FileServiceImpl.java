package com.jt.service;


import com.jt.vo.ImageVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Service
@PropertySource("classpath:/properties/image.properties")
public class FileServiceImpl implements FileService {
    //创建虚拟路径
    @Value("${image.urlPath}")
    String urlPath ;//= "http://image.jt.com";
    //创建根目录
    @Value("${image.localDirPath}")
    private String localDirPath ;//="D:/JT-SOFT/images";
    private  static Set<String> imageTypeSet = new HashSet<>();
    static {
        imageTypeSet.add(".jpg");
        imageTypeSet.add(".png");
        imageTypeSet.add(".gif");
    }

    /**
     *
     * 1.校验文件有效性. jpg/. png|. gif......
     * 2.校验文件是否为恶意程序 (木马. exe).jpg
     * 3.提高用户检索图片的效率  分目录存储.
     * 4.为了防止重名图片的提交自定义文件名称.
     * 5.实现图片的物理上传 本地磁盘中.
     * 6.准备一个访问图片的虚拟路径
     *  @param uploadFile
     * @return
     */

    @Override
    public ImageVO upload(MultipartFile uploadFile) {
        //1 校验图片类型 1) 利用正则表达式进行校验 2)指定一个集合进行校验
        //1.1获取文件名称
        String fileName= uploadFile.getOriginalFilename();
        fileName = fileName.toLowerCase();
        //1.2获取文件名称后缀,然后将获取的后缀转为小写
        String fileType = fileName.substring(fileName.lastIndexOf("."));

        //进行校验
        if (!imageTypeSet.contains(fileType)){
            //如果类型不匹配,应该告诉用户上传图片有误
            //
            return ImageVO.fail();
        }
        //.校验文件是否为恶意程序 (木马. exe).jpg 方法:通过图片的特有属性进行校验
        try {
           //2.1  将上传的图片利用图片API进行转化,如果不能成功转化则一定不是图片
            BufferedImage bufferedImage = ImageIO.read(uploadFile.getInputStream());
            //校验图片的特有属性 宽度和高度
            int width = bufferedImage.getWidth();
            int height = bufferedImage.getHeight();
            if (width==0 || height == 0){
                //如果获取的宽或者高为0 则不是图片
                return  ImageVO.fail();
            }

        } catch (IOException e) {
            e.printStackTrace();
            return  ImageVO.fail();
        }
        //实现分目录存储 方案1 利用hash 之后每隔2-3位进行
        //方案2 通过时间来划分目录
        //获取格式化时间 利用工具API
        String datePath = new SimpleDateFormat("/yyyy/MM/dd/").format(new Date());
        //创建文件目录 2部分 根目录+时间目录
        String localDir = localDirPath+datePath;
        File dirFile = new File(localDir);
        if (!dirFile.exists()){
            dirFile.mkdirs();
        }
        //4防止文件重名,需要自定义名称 UUID
        //4.1 生成uuid
        String uuid = UUID.randomUUID().toString().replace("-","");
        //4.2 动态生成文件名称
        String uuidFileName = uuid+fileType;
        //5 实现文件上传 准备文件全路径
        String realFilePath = localDir+uuidFileName;
        //5.1 封装文件真实对象
        File imageFile = new File(realFilePath);
        //5.2 实现文件上传
        try {
            uploadFile.transferTo(imageFile);

        } catch (IOException e) {
            e.printStackTrace();
            return  ImageVO.fail();
        }

        //6.暂时使用虚拟路径
        //拼接指定的虚拟路径
        String url =urlPath+datePath+uuidFileName;
        return ImageVO.success(url);
    }
}
