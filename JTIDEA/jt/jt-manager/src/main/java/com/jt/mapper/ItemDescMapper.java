package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.ItemDesc;
//继承接口并且要指定对应的表
public interface ItemDescMapper extends BaseMapper<ItemDesc> {//对象与表进行绑定
    //如果自己不写sql,则可以省略mapper映射文件的编写
    void deleteItemDescByIds(Long[] ids);

}
