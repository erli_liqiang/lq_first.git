package com.jt.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;

@Configuration
public class MybatisPlusConfig {
	@Bean//表示将分页的拦截器交给spring容器管理
	public PaginationInterceptor paginationInterceptor() {
		return new PaginationInterceptor();
	}
	
}
