package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.annotation.CacheFind;
import com.jt.utils.ObjectMapperUtils;
import com.jt.vo.EasyUITree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jt.mapper.ItemCatMapper;
import com.jt.pojo.ItemCat;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemCatServiceImpl implements ItemCatService {
	@Autowired
	private ItemCatMapper itemCatMapper;
	@Autowired(required = false) //告知spring容器,该注入不是必须的,如果该对象被程序调用时,则生效
	private Jedis jedis;

	//通过缓存实现
	@Override
	public List<EasyUITree> findItemCatByCache(Long parentId) {
		//定义一个key
		String key ="ITEM_CAT_LIST::"+parentId;
		//判断key是否存在于redis中
		List<EasyUITree> treeList = new ArrayList<>();
		Long start = System.currentTimeMillis();
		//存在则从key中获取数据
		if (jedis.exists(key)){
			String json = jedis.get(key);
			 treeList = ObjectMapperUtils.toObject(json, treeList.getClass());
			long end = System.currentTimeMillis();
			System.out.println("redis查询的时间为:"+(end-start));
		}else {
		//不存在则从数据库获取,并将获取的数据存入到redis
			treeList = findEasyUITree(parentId);
			String json = ObjectMapperUtils.toJSON(treeList);
			long end = System.currentTimeMillis();
			jedis.set(key,json);
			System.out.println("数据库查询的时间为:"+(end-start));
		}
		return treeList;
	}
	@CacheFind(key = "ITEM_CAT_NAME")
	@Override
	public ItemCat findItemCatById(Long itemCatId) {
		//利用MP机制,查询数据库数据
		return itemCatMapper.selectById(itemCatId);
	}

	/**
	 * 	根据接口添加实现类的方法 根据父级id查询商品分类
	 * 	思路: 先将所有的商品分类信息查询出来,存到一个list集合中,
	 * 	然后创建一个新的list,类型为EasyUITree,
	 * 	用来封装需要的信息(id ,text(name),state,),然后返回该list
	 * 	业务思路:
	 * 		1 根据用户传递的parentId
	 * 		2 可以查询ItemCat数据对象的信息
	 * 		3 动态的将ItemCat的对象转化为EasyUITree对象  ~~~~核心步骤
	 * 		4 返回值要求是List<EasyUITree>
	 * @param parendId
	 * @return
	 */
	@Override
	@CacheFind(key = "ITEM_CAT_LIST")
	public List<EasyUITree> findEasyUITree(Long parendId) {

		QueryWrapper<ItemCat> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("parent_id",parendId);
		List<ItemCat> itemCatList = itemCatMapper.selectList(queryWrapper);
		List<EasyUITree> easyUITrees = new ArrayList<>();
		//将数据一个一个的进行格式转化
		for (ItemCat  itemCat:itemCatList
			 ) {
			Long id = itemCat.getId();//获取id
			String text = itemCat.getName();//获取文本
			//如果是父级 则默认应该处于关闭状态 closed,否则应该处于打开状态 open
			String state = itemCat.getIsParent()?"closed":"open";//获取状态信息
			//通过构造方法为vo对象赋值,实现了数据的转换
			EasyUITree easyUITree = new EasyUITree(id,text,state);//也可以通过set方法进行赋值
			easyUITrees.add(easyUITree);
		}
		//ctrl+F9直接热部署
		return easyUITrees;
	}

}
