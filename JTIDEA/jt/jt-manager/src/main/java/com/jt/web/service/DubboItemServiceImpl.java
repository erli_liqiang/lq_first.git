package com.jt.web.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.jt.annotation.CacheFind;
import com.jt.mapper.ItemDescMapper;
import com.jt.mapper.ItemMapper;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.service.DubboItemService;
import org.springframework.beans.factory.annotation.Autowired;

@Service //导入dubbo的包
public class DubboItemServiceImpl implements DubboItemService {
    @Autowired
    private ItemMapper itemMapper;
    @Autowired
    private ItemDescMapper itemDescMapper;

    @Override
    @CacheFind(key="ITEM_ID")
    public Item queryItemById(Long itemId) {
        Item item = itemMapper.selectById(itemId);

        return item;
    }

    @Override
    @CacheFind(key="ITEM_DESC_ID")
    public ItemDesc queryItemDescById(Long itemId) {
        ItemDesc itemDesc = itemDescMapper.selectById(itemId);
        return itemDesc;
    }
}
