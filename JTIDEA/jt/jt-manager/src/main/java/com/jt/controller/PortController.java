package com.jt.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class PortController {
    @Value("${server.port}")
    private Integer port;
    @RequestMapping("/getPort")
    public String getPort(HttpServletRequest request){
        int port =request.getLocalPort();
        return "当前的端口为"+port;
    }


}
