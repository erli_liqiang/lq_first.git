package com.jt.service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jt.mapper.ItemDescMapper;
import com.jt.pojo.ItemDesc;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.ItemMapper;
import com.jt.pojo.Item;
import com.jt.vo.EasyUITable;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ItemServiceImpl implements ItemService {
	
	@Autowired
	private ItemMapper itemMapper;
	@Autowired
	private ItemDescMapper itemDescMapper;
	//根据指定的ID查询商品详情信息
	@Override
	public ItemDesc findItemDescById(Long itemId) {
		ItemDesc itemDesc = itemDescMapper.selectById(itemId);
		return itemDesc;
	}

	//利用MP
	@Override
	public EasyUITable findItemByPage(Integer page, Integer rows) {
		QueryWrapper<Item> queryWrapper = new QueryWrapper<>();
		queryWrapper.orderByDesc("updated");
		//Page 参数1: page 表示页码值 参数2 : rows 表示页面大小
		IPage<Item> iPage = new Page<>(page, rows);
		//根据分页模型执行分页操作,并将结果返回给分页对象
		iPage = itemMapper.selectPage(iPage, queryWrapper);
		long total = iPage.getTotal();//获取总记录
		List<Item> itemList = iPage.getRecords();//获取当前页记录
		return new EasyUITable(total, itemList);
	}
	//保存商品 完成两张表的入库操作,保证商品的id与商品详情id一致
	//注意事项: 完成数据库数据更新操作时,需要注意数据库事务问题
	@Transactional
	@Override
	public void saveItem(Item item , ItemDesc itemDesc) {
		//保存信息时要记录更改时间
//		item.setStatus(1).setCreated(new Date()).setUpdated(item.getCreated());
		item.setStatus(1);
		//典型一对一的插入
		itemMapper.insert(item);
		//分析问题: item表的 主键是自增数据库入库之后才会有 主键生成.
		//解决方案:让数据库完成入库之后自动的实现主键的回显。改操作由MP机制动态完成
		//<insert id="xxx" keyProperty="id" useGeneratedKeys="true"></insert>
		itemDesc.setItemId(item.getId());
		itemDescMapper.insert(itemDesc);

	}
	//修改商品信息
	@Transactional
	@Override
	public void updateItem(Item item , ItemDesc itemDesc) {
		item.setStatus(1);
//		item.setStatus(1).setCreated(new Date()).setUpdated(item.getCreated());
		itemMapper.updateById(item);//设定主键
		itemDesc.setItemId(item.getId());
		//实现商品详情的更新
		itemDescMapper.updateById(itemDesc);
	}
	//删除商品信息
	@Transactional
	@Override
	public void deleteItemsByIds(Long[] ids) {
		//将数组转化为集合
//		List<Long> idsList = Arrays.asList(ids);
//		itemMapper.deleteBatchIds(idsList);
		itemMapper.deleteItems(ids);
		//根据商品id删除商品的详情信息
		itemDescMapper.deleteItemDescByIds(ids);
	}

	//利用手写sql实现状态修改
	@Override
	public void updatStatus(Long[] ids, Integer status) {

		itemMapper.updown(ids,status);

	}

	//利用MP实现状态修改(上下架操作)
//	@Override
//	public void updatStatus(Long[] ids, Integer status) {
//		List<Long> idsList = Arrays.asList(ids);//数组转化为集合
//		UpdateWrapper<Item> updateWrapper = new UpdateWrapper<>();
//
//		updateWrapper.in("id",idsList);
//		Item item = new Item();
//		item.setStatus(status);
//		itemMapper.update(item,updateWrapper);
//
//	}

	//执行步骤:手动编辑sql 2 利用MP自动生成
//	@Override
//	public EasyUITable findItemByPage(Integer page, Integer rows) {
//		
//		
//		//查询记录总数
//		long total = itemMapper.selectCount(null);
//		//计算起始索引
//		int start = (page-1)*rows;
//		//查询当前页记录总数
//		List<Item> itemList=itemMapper.findItemByPage(start,rows);
//		//参数1 : 记录的总数 参数2 : 当前页的记录
//		return new EasyUITable(total, itemList);
//	}
	
	

	
	
	
	
	
	
	
	
	
}
