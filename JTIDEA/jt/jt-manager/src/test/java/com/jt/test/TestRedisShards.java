package com.jt.test;

import org.junit.jupiter.api.Test;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;

import java.util.ArrayList;
import java.util.List;

//该类表示测试redis分片机制
public class TestRedisShards {

    /**
     *  说明 : 在linux中有3台redis,需要通过程序进行动态的连接
     *  实现数据的存储
    */
    @Test
    public void  test01(){
        List<JedisShardInfo> shards = new ArrayList<>();
        shards.add(new JedisShardInfo("192.168.126.129",6379));
        shards.add(new JedisShardInfo("192.168.126.129",6380));
        shards.add(new JedisShardInfo("192.168.126.129",6381));
        //分片对象
        ShardedJedis shardedJedis = new ShardedJedis(shards);
        shardedJedis.set("吃鸡","救救我");
        System.out.println(shardedJedis.get("吃鸡"));
    }

}
