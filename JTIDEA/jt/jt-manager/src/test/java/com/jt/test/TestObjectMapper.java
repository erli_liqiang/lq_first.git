package com.jt.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jt.pojo.ItemDesc;
import com.jt.utils.ObjectMapperUtils;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class TestObjectMapper {


    private  static  final ObjectMapper MAPPER = new ObjectMapper();
    @Test
    public  void test01(){
        ItemDesc itemDesc = new ItemDesc();
        itemDesc.setItemId(100L).
                setItemDesc("测试JSON对象转字符串").
                setCreated(new Date()).
                setUpdated(itemDesc.getCreated());

        try {
            //将java对象转化为json
            String json = MAPPER.writeValueAsString(itemDesc);
            System.out.println(json);
            //将json转化为对象 利用反射机制,利用set/get方法为对象赋值
            ItemDesc itemDesc1 = MAPPER.readValue(json, ItemDesc.class);
            System.out.println(itemDesc1);//只输出当前对象的数据, 不负责父级的,因为data注解重写了toString方法


        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


    }

    @Test
    public  void testObjectMapperUtils(){
        ItemDesc itemDesc = new ItemDesc();
        itemDesc.setItemId(100L).
                setItemDesc("测试JSON对象转字符串").
                setCreated(new Date()).
                setUpdated(itemDesc.getCreated());

        String json = ObjectMapperUtils.toJSON(itemDesc);
        System.out.println(json);
        ItemDesc itemDesc1 = ObjectMapperUtils.toObject(json, ItemDesc.class);
        System.out.println(itemDesc1);

    }


}
