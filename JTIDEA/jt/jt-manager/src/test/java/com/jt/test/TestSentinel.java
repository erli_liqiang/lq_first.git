package com.jt.test;

import org.junit.jupiter.api.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisSentinelPool;

import java.util.HashSet;
import java.util.Set;

//主要完成哨兵的测试
public class TestSentinel {

    @Test
    public void  test01(){
        Set<String> sentinel = new HashSet<>();
        //定义一个哨兵
        String node = "192.168.126.129:26379";
        sentinel.add(node);

        JedisSentinelPool sentinelPool = new JedisSentinelPool("mymaster",sentinel);

        Jedis jedis = sentinelPool.getResource();
        jedis.set("kk","redis哨兵配置成功");
        System.out.println(jedis.get("kk"));

    }


}
