package com.jt.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;
import redis.clients.jedis.params.SetParams;

import java.util.Map;

@SpringBootTest //需要依赖spring容器进行操作,从容器中动态的获取对象
public class TestRedis {
    @Autowired
    private  Jedis jedis;

    /**
     * 完成redis入门案例的测试
     */
    @Test
    public void test01(){
//        Jedis jedis = new Jedis("192.168.126.129",6379);
        //想redis中存储数
        jedis.set("2004","今天没下雨,尴尬");
        String value = jedis.get("2004");
        System.out.println(value);

    }

    /**
     *
     */
    @Test
    public void test02(){
        Jedis jedis = new Jedis("192.168.126.129",6379);
        //判断redis中是否存在key
        if (jedis.exists("2004")){
            //如果存在则设置超时时间
            jedis.expire("2004",100);
            //线程休眠
            try {
                Thread.sleep(2000);
                //获取剩余的存活时间
                Long time = jedis.ttl("2004");
                System.out.println("还能活"+time);
                //撤销超时时间
                jedis.persist("2004");

            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }

    }

    /**
     * 需求说明:
     * 1.redis.set操作,后面的操作会将之前的value覆盖
     * 要求:如果key已经存在,则不允许赋值
     * 环境问题: 检测redis中是否已经存在该数据
     */
    @Test
    public void test03() {
        Jedis jedis = new Jedis("192.168.126.129", 6379);
        jedis.flushAll();
        jedis.setnx("boos","liq");
        jedis.setnx("boss","zpk");
        System.out.println(jedis.get("boss"));
    }

    /**
     * 为数据添加超时时间,保证原子性操作
     *
     * 锁机制: 保证原子性
     * 小结: setnx
     *      setex
     */
    @Test
    public void test04() {
        Jedis jedis = new Jedis("192.168.126.129", 6379);
        jedis.setex("aaa",20,"xxxxxx");//满足原子性需求


    }

    /**
     *  需求:
     *      1 要求赋值操作时,如果数据已经存在,则不允许赋值
     *      2 同时要求添加超时时间,并且满足原子性需求
     *      private static final String XX = "xx";只 有key存在时，才能赋值
     *      private static final String NX = "nx";只有key不存在时,赋值
     *
     *       private static final String PX = "px"; 亳秒
             private static final String EX = "ex";秒
     */
    @Test
    public void test05() {
        Jedis jedis = new Jedis("192.168.126.129", 6379);
        SetParams params = new SetParams();
        params.nx().ex(10);
        jedis.set("AAA","CCC",params);
        System.out.println(jedis.get("AAA"));

    }

    /**
     *  测试hash数据类型
     */
    @Test
    public void test06() {
        Jedis jedis = new Jedis("192.168.126.129", 6379);
        jedis.hset("person","name","tomcat");
        jedis.hset("person","age","18");
        Map<String, String> person = jedis.hgetAll("person");
        System.out.println(person);
        jedis.hsetnx("user","id","1");
        jedis.hsetnx("user","name","tom");
        Map<String, String> user = jedis.hgetAll("user");
        System.out.println(user);
    }
    /**
     *  测试list集合数据类型
     */
    @Test
    public void test07() {
        Jedis jedis = new Jedis("192.168.126.129", 6379);
       jedis.lpush("list","1","2","3","4","5");
        String value = jedis.rpop("list");
        System.out.println(value);
    }

    /**
     * 实现redis的事务控制
     */
    @Test
    public void testMulti() {
        Jedis jedis = new Jedis("192.168.126.129", 6379);
        Transaction transaction = jedis.multi();//开启
        try{

        transaction.set("AAA","123");
        transaction.set("BBB","123");
        transaction.exec();//提交
        }catch (Exception e){
            transaction.discard();//回滚
        }
        String aaa = jedis.get("AAA");
        System.out.println(aaa);

    }

}
