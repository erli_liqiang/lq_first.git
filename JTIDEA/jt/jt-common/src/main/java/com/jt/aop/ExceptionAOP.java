package com.jt.aop;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@Aspect
public class ExceptionAOP {

    @AfterThrowing(value = "bean(sysExceptionAOP)",throwing = "e")
    public void  around(JoinPoint joinPoint,Throwable e){
        //获取目标类型
        Object target = joinPoint.getTarget();
        //获取目标方法
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        String methodName = methodSignature.getMethod().getName();
        //获取目标参数
        String args = Arrays.toString(joinPoint.getArgs());
        //获取报错信息
        String message = e.getMessage();

        //封装异常信息
        String finalMessage = target+"--"+methodName+"--"+args+"--"+message;


    }


}
