package com.jt.aop;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.jt.vo.SysResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestControllerAdvice //定义异常处理的通知 只拦截controller层抛出的异常
public class SysExceptionAOP {
    /**
     *  如果跨域访问发生了异常,返回结果必须要进行特殊格式的处理
     *  如果是跨域的访问形式,全局的异常处理可以正确的返回结果
     *  callback(json)
     *
     *  思路: 判断用户提交的参数中是否有callback参数
     * @param exception
     * @return
     */
    @ExceptionHandler(RuntimeException.class)
    public Object systemResultException(Exception exception, HttpServletRequest request){

        //获取用户请求的参数
        String callback = request.getParameter("callback");
        //判断是否为跨域
        if (StringUtils.isEmpty(callback)){
            //用户请求不是jsonp跨域访问形式
            exception.printStackTrace();
            log.error("{~~~~~"+exception.getMessage()+"}",exception);
            return  SysResult.fail();

        }else {
            //jsonp的报错信息.
            exception.printStackTrace();
            return  new JSONPObject(callback,  SysResult.fail());
        }


    }
}
