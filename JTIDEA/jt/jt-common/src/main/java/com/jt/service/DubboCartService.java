package com.jt.service;

import com.jt.pojo.Cart;

import java.util.List;

public interface DubboCartService {
    //查询购物车列表
    List<Cart> findCartListByUserId(Long userId);
    //修改购物车数量
    void updateCatNum(Cart cart);
    //删除购物车
    void deleteCart(Cart cart);
    //新增购物车
    void addCart(Cart cart);
    //修改购物车数量
//    void updateCatNum(Long itemId, Integer itemNum, Long userId);
}
