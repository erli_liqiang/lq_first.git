package com.jt.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
@Retention(value = RetentionPolicy.RUNTIME)//标识运行时有效
@Target(ElementType.METHOD)//标识作用于方法上
public @interface CacheFind {
    //1.设定key
    public String  key();
    //2.超时时间
    public int seconds() default  0;

}
