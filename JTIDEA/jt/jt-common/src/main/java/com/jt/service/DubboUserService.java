package com.jt.service;

import com.jt.pojo.User;

public interface DubboUserService {
    //保存用户的方法
    void saveUser(User user);
    //登录操作
    String doLogin(User user);
}
