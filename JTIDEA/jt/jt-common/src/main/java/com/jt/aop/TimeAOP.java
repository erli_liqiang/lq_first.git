package com.jt.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
@Aspect
public class TimeAOP {
    //定义一个切入点表达式
    @Pointcut("bean(*ServiceImpl)")
    public void timePoint(){}
//    @Before("timePoint()")
//    public void after(){
//        System.out.println("时间监控的前置通知");
//    }
    @Around("timePoint()")
    public Object around(ProceedingJoinPoint joinPoint){
        //获取class名称
        String className = joinPoint.getSignature().getDeclaringTypeName();
        //获取方法名称
        MethodSignature methodSignature =(MethodSignature)joinPoint.getSignature();
        String methodName = methodSignature.getName();
        Object result =null;
        long startTime = System.currentTimeMillis();
        try {
            
            result =joinPoint.proceed();

            long endTime = System.currentTimeMillis();
            
            long totalTime = endTime-startTime;
            //将程序的执行时间写出到文件中
            File file = new File("D://serverTime.txt");

            String str = className+"."+methodName+":"+totalTime+"ms";
            BufferedWriter bw = new BufferedWriter(new FileWriter(file,true));
            bw.write(str);
            bw.newLine();
            bw.close();


        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

            return  result;

    }


}
