package com.jt.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration //标识当前类是一个配置类
public class CorsConfig implements WebMvcConfigurer {

    //扩展跨域请求的方法
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // 1 运行什么样的请求进行跨域
         // /* 只允许一级目录的请求 /** 表示多级的目录可以请求访问
        registry.addMapping("/**")
                .allowedOrigins("*") //允许哪些服务进行跨域
                .allowCredentials(true)//表示是否可以携带cookie进行跨域
                .allowedMethods("GET","POST","PUT","DELETE","HEAD") //可以允许访问的请求方式
                .maxAge(1800);//定义探针的检测时间 在规定的时间内不再询问是否允许跨域


    }
}
