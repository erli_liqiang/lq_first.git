package com.jt.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 *  改vo对象是系统返回值对象,主要包括三个属性
 *  1 定义状态码 200 表示执行成功 201表示执行失败  人为定义,与浏览器没有关系
 *  2 定义返回值信息, 服务器可能会给用户一些提示信息,
 *  3 定义返回值结果对象,服务器在后端处理完成业务之后,将对象返回给前端
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class SysResult {
    //定义状态码
    private  Integer status;
    //定义状态码信息
    private  String msg;
    //定义返回值结果对象,返回业务数据
    private  Object data;

    //通过构造方法,封装结果数据
    //失败返回的数据
    public static SysResult fail(){
        return new SysResult(201,"业务调用失败",null);
    }
    public static SysResult success(){//只标识成功,不携带数据
        return new SysResult(200,"业务调用成功",null);
    }

    /**
     * 写工具API的时候切记方法重载不要出现耦合
     * @param data
     * @return
     */
    public static SysResult success(Object data){//成功之后,返回业务数据
        return new SysResult(200,"业务调用成功",data);
    }
    public static SysResult success(String msg,Object data){//成功之后返回状态信息以及业务数据
        return new SysResult(200,msg,data);
    }

}
