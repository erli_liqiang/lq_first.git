package com.jt.service;

import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;

//定义item中立的接口
public interface DubboItemService {


    Item queryItemById(Long itemId);

    ItemDesc queryItemDescById(Long itemId);
}
