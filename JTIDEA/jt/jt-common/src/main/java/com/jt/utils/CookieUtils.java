package com.jt.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//cookie工具类
public class CookieUtils {

    //新增cookie
    public static  void addCookie(String name, String value, String path , String doMain , int maxAge , HttpServletResponse response){
        //创建cookie对象
        Cookie cookie = new Cookie(name,value);
        cookie.setPath(path);
        cookie.setDomain(doMain);
        cookie.setMaxAge(maxAge);
        response.addCookie(cookie);

    }
    //删除cookie
    public static void deleteCookie(String name, String path, String domain, HttpServletResponse response){
        //校验自己完成
        Cookie cookie = new Cookie(name,"");
        cookie.setPath(path);
        cookie.setDomain(domain);
        cookie.setMaxAge(0);    //后期维护使用枚举
        response.addCookie(cookie);
    }
    //根据cookie的name属性查询cookie对象
    public static  Cookie getCookieByName(HttpServletRequest request,String name){
        Cookie[] cookies = request.getCookies();
        //判断cookies是否为空
        if (cookies!=null && cookies.length>0){
            for (Cookie cookie : cookies) {
                if (name.equalsIgnoreCase(cookie.getName())){
                    return  cookie;
                }

            }

        }
        return  null;

    }


}
