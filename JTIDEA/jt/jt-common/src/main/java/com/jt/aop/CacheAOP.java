package com.jt.aop;

import com.jt.annotation.CacheFind;
import com.jt.utils.ObjectMapperUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.ShardedJedis;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

@Component //标识为一个javaBean
@Aspect //标识为一个切面
public class CacheAOP {
    //注入redis配置
    @Autowired
    private JedisCluster jedis; // redis集群
//    private Jedis jedis;  //单台redis
//    private ShardedJedis jedis; //redis分片
    /**
     * AOP缓存业务的实现
     * 1 切入点表达式应该拦截自定义的注解  @CacheFind
     * 2 通知方法: 环绕通知 @Around
     * 注意事项:如果使用环绕通知,则必须在第一个参数的位置添加ProceedingJoinPoint
     */
    @Around("@annotation(cacheFind)")
    public  Object around(ProceedingJoinPoint joinPoint, CacheFind cacheFind){

        try {
            Object result=null;
            //1.动态获取注解中的值
            String prekey = cacheFind.key();
            //获取方法中的参数
            String args = Arrays.toString(joinPoint.getArgs());
            //构建key
            String key = prekey+ "::"+args;
            //检查redis中是否有数据
            if (jedis.exists(key)){
                //缓存中有数据
                System.out.println("redis缓存查询");
                String json = jedis.get(key);
                //动态获取目标方法的返回值类型
                MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
                Class<?> returnType = methodSignature.getReturnType();
                //将缓存中取出来的数据转化为对象
                result = ObjectMapperUtils.toObject(json,returnType );

            }else {
                //缓存中没有数据
                System.out.println("AOP数据库查询");
                //执行目标方法
                result = joinPoint.proceed();
                //将结果转化为json串
                String json = ObjectMapperUtils.toJSON(result);
                //判断缓存时候设置了超时时间
                if (cacheFind.seconds()>0)
                    //设置超时时间
                 jedis.setex(key,cacheFind.seconds(),json);
                else
                    //不设置超时时间
                    jedis.set(key,json);

            }
            return  result;

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            throw  new RuntimeException(throwable);//将检测异常转化为运行时异常
        }

    }







//    //1.定义一个切入点表达式
//    @Pointcut("bean(itemCatServiceImpl)")//只拦截xxx类中的方法
//    public void pointCut(){}
//    //2.定义通知方法
//    @Before("pointCut()")
//    public  void  before(JoinPoint point){
//        System.out.println("我是前置通知方法");
//        //获取方法名称
//        MethodSignature methodSignature =(MethodSignature) point.getSignature();
//        String methodName = methodSignature.getMethod().getName();
//        //获取类名
//        String className = point.getSignature().getDeclaringTypeName();
//        //获取目标方法对象
////        String name = point.getTarget().getClass().getName();
//        Object target = point.getTarget();
//        //获取参数
//        Object[] objs = point.getArgs();
//        System.out.println("方法名称"+methodName);
//        System.out.println("标方法对象"+target);
//        System.out.println("获取参数"+ Arrays.toString(objs));
//
//    }
//
//    //后置通知
//    @After("pointCut()")
//    public void after(){
//        System.out.println("我是后置通知");
//    }
//    //返回通知
//    @AfterReturning("pointCut()")
//    public void afterReturning(){
//        System.out.println("我是返回通知");
//    }




}
