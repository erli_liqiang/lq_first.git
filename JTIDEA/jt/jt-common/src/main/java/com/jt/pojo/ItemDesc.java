package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

//商品详情pojo对象
@TableName("tb_item_desc")
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class ItemDesc extends  BasePojo {
    @TableId             //只标识主键
    private  Long itemId;//要求与商品表id一致
    private String itemDesc; //商品详情信息


}
