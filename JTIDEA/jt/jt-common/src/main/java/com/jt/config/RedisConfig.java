package com.jt.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import redis.clients.jedis.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//表明该类是一个配置类
@Configuration
@PropertySource("classpath:properties/redis.properties")
public class RedisConfig {
    @Value("${redis.nodes}")
    private String nodes;

    /**
     * spring 整合redis集群
     */
    @Bean  //一定要交给spring容器管理
    public JedisCluster jedisCluster(){
        Set<HostAndPort> nodeSet = new HashSet<>();
        String[] clusters = nodes.split(",");//形成node结点的数组
        for (String cluster : clusters) {//遍历结点
            String host = cluster.split(":")[0];//获取ip
            int port = Integer.parseInt(cluster.split(":")[1]);//获取端口
            HostAndPort hostAndPort = new HostAndPort(host,port);
            nodeSet.add(hostAndPort);
        }
        return  new JedisCluster(nodeSet);

    }



    /**
     * spring 整合多台redis
     */
//    @Bean
//    public ShardedJedis shardedJedis(){
//        //获取结点信息
//        String[] nodeInfo = nodes.split(",");
//
//        List<JedisShardInfo> shards = new ArrayList<>();
//        //遍历所有的结点
//        for (String node:nodeInfo) {
//            //获取host,port
//            String host = node.split(":")[0];
//            int port = Integer.parseInt(node.split(":")[1]);
//            JedisShardInfo info = new JedisShardInfo(host,port);
//            shards.add(info);
//        }
//        //分片对象
//        return   new ShardedJedis(shards);
//    }


//    @Value("${redis.host}")
//    private String host;
//    @Value("${redis.port}")
//    private Integer port;
//
//    @Bean
//    public Jedis jedis(){
//
//        return  new Jedis(host,port);
//    }

}
