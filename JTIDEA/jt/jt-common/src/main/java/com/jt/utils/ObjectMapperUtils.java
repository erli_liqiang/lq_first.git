package com.jt.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectMapperUtils {

    //1. 创建工具API对象
    private static final ObjectMapper MAPPER = new ObjectMapper();

    //2.封装API 将对象转化为JSON
    public static  String toJSON(Object object){
        if (object==null ){
            throw  new RuntimeException("对象不能为null");
        }

        try {
            String json = MAPPER.writeValueAsString(object);
            return  json;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw  new RuntimeException(e.getMessage());
        }

    }

    //3.封装API 将JSON转化为对象
    public static  <T> T toObject(String json,Class<T> target){
        //判断json串
        if (json==null || "".equals(json) || target==null){
            throw  new RuntimeException("参数不能为null");
        }
        try {
            T t = MAPPER.readValue(json,target);
            return  t;

        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw  new RuntimeException(e.getMessage());
        }

    }

}

