package com.jt.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jt.mapper.CartMapper;
import com.jt.pojo.Cart;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class DubboCartServiceImpl implements  DubboCartService {
    @Autowired
    private CartMapper cartMapper;
    //根据userId 查询购物车记录
    @Override
    public List<Cart> findCartListByUserId(Long userId) {
        QueryWrapper<Cart> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id",userId);
        List<Cart> cartList = cartMapper.selectList(queryWrapper);
        return cartList;
    }
    //修改购物车数量
    @Override
    public void updateCatNum(Cart cart) {
        Cart cart1 = new Cart();
        cart1.setNum(cart.getNum());
        UpdateWrapper<Cart> updateWrapper = new UpdateWrapper<>();
        //根据itemId和userId确定唯一购物
        updateWrapper.eq("user_id",cart.getUserId()).eq("item_id",cart.getItemId());
        cartMapper.update(cart1,updateWrapper);
    }
    //删除购物车
    @Override
    public void deleteCart(Cart cart) {
        //将对象中不为null的对象作为where条件
        cartMapper.delete(new QueryWrapper<Cart>());
    }
    //新增购物车
    //如果重复添加,则更新数量
    //如何判断为是否为重复添加?根据user_id/item_id
    @Override
    public void addCart(Cart cart) {
        //先要查询用户是否已经加购
        QueryWrapper<Cart> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("item_id",cart.getItemId())
                .eq("user_id",cart.getUserId());

        Cart cartDB = cartMapper.selectOne(queryWrapper);
        if (cartDB==null){
            //表示用户第一次新增购物车,
            cartMapper.insert(cart);

        }else {
            //表示重复加购,只做数量的修改
            Cart cart1 = new Cart();
            //如果用户已经加入过购物车,则需要在原有数量的基础上进行数量修改
            cart1.setNum(cartDB.getNum()+cart.getNum()).setId(cartDB.getId());
            //主键充当where条件,对象中其他不为null的属性当作set的值
            cartMapper.updateById(cart1);
            //sql

        }

    }
}
