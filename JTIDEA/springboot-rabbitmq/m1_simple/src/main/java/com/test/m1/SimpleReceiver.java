package com.test.m1;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "helloworld")
public class SimpleReceiver {
    @RabbitHandler
    public void  receive(String msg){
        System.out.println("收到消息"+msg);
    }

}
