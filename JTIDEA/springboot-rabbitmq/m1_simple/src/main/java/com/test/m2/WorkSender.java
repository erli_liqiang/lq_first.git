package com.test.m2;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class WorkSender {
    @Autowired
    private AmqpTemplate amqpTemplate;

    public  void send(){
        while (true){
            System.out.println("输入:");
            String msg = new Scanner(System.in).nextLine();
            //spring 默认将消息的 DeliveryMode 设置为 PERSISTENT 持久化,
            amqpTemplate.convertAndSend("task_quque",msg);
        }
    }
}
