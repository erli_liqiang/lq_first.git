package com.test;

import org.springframework.amqp.core.Queue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Main {
    public static void main(String[] args) {

        SpringApplication.run(Main.class,args);

    }

    @Bean
    public Queue task_queue() {
        /*
         * 可用以下形式:
         * new Queue("helloworld") - 持久,非排他,非自动删除
         * new Queue("helloworld",false,false,false,null)
         */
        return new Queue("task_queue");
    }
}
