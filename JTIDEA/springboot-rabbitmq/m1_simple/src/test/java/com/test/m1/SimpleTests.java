package com.test.m1;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Scanner;

@SpringBootTest
public class SimpleTests {
    @Autowired
    private  SimpleSender simpleSender;

    @Test
    void test1() throws Exception {
        simpleSender.send();
        System.out.println("[按回车结束]");
        new Scanner(System.in).nextLine();
    }

}
