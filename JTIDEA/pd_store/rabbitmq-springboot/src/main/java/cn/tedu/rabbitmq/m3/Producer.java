package cn.tedu.rabbitmq.m3;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;
//发布订阅模式
@Component
public class Producer {
    @Autowired
    private AmqpTemplate amqpTemplate;

    public  void send(){
        while (true){
            System.out.println("输入消息");
            String msg = new Scanner(System.in).nextLine();
            amqpTemplate.convertAndSend("logs","",msg);
        }
    }

}
