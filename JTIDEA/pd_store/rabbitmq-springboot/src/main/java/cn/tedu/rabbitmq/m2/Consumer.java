package cn.tedu.rabbitmq.m2;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class Consumer {
    @RabbitListener(queues = "task_queue")
    public void receiver1(String s){

        System.out.println("消费者1收到"+s);
    }

    @RabbitListener(queues = "task_queue")
    public void receiver2(String s){

        System.out.println("消费者2收到"+s);
    }
}
