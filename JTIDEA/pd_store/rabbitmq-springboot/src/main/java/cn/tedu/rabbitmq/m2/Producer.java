package cn.tedu.rabbitmq.m2;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class Producer {
    @Autowired
    private AmqpTemplate amqpTemplate;

    public  void send(){
        while (true){
            System.out.println("输入消息");
            String msg = new Scanner(System.in).nextLine();
            amqpTemplate.convertAndSend("task_queue",msg);
            System.out.println("消息已经发送");
//            MessagePostProcessor 在消息发送之前先执行
            //在预处理器中,可以对消息先做一些设置,设置后再发送
//            amqpTemplate.convertAndSend("task_queue", (Object) msg, new MessagePostProcessor() {
//                @Override
//                public Message postProcessMessage(Message message) throws AmqpException {
//                    MessageProperties properties = message.getMessageProperties();
//                    properties.setDeliveryMode(MessageDeliveryMode.NON_PERSISTENT);//非持久消息
//
//                    return message;
//                }
//            });
        }
    }

}
