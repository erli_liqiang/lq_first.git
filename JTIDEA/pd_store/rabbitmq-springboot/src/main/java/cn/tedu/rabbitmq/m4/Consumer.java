package cn.tedu.rabbitmq.m4;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class Consumer {
    //交换机 随机队列  使用绑定键绑定
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange(name = "direct_logs",declare = "false"),
            value =@Queue, //若无参数 则为随机队列 非持久 独占 自动删除
            key = {"error"}
    ))
    public void receiver1(String s){

        System.out.println("消费者1收到"+s);
    }

    //交换机 随机队列  使用绑定键绑定
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange(name = "direct_logs",declare = "false"),
            value =@Queue, //若无参数 则为随机队列 非持久 独占 自动删除
            key = {"error","info","warning"}
    ))
    public void receiver2(String s){

        System.out.println("消费者2收到"+s);
    }
}
