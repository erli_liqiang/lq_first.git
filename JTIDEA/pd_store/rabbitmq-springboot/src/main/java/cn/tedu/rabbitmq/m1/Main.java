package cn.tedu.rabbitmq.m1;

import org.springframework.amqp.core.Queue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.swing.*;

@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class,args);
    }
    @Bean
    public Queue helloworldQueue(){
//        return  new Queue("helloworld"); //true false false
        return  new Queue("helloworld",false);
    }
}
