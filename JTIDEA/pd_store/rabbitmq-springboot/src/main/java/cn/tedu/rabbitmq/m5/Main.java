package cn.tedu.rabbitmq.m5;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

//路由模式
@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class,args);
    }


    //定义一个交换机
    @Bean
    public TopicExchange directLogsExchange(){

        return  new TopicExchange("topic_logs",false,false);//非持久 不自动删除

    }
}
