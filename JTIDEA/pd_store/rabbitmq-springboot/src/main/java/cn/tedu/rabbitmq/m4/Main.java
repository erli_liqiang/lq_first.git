package cn.tedu.rabbitmq.m4;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
//路由模式
@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class,args);
    }


    //定义一个交换机
    @Bean
    public DirectExchange directLogsExchange(){

        return  new DirectExchange("direct_logs",false,false);//非持久 不自动删除

    }
}
