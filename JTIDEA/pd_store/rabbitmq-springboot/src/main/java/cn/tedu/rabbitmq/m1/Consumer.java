package cn.tedu.rabbitmq.m1;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "helloworld")
public class Consumer {
    @RabbitHandler
    public void receiver(String s){
        System.out.println("收到"+s);
    }

}
