package cn.tedu.sp02.item.controller;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.service.ItemService;
import cn.tedu.web.util.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

@Slf4j
@RestController
public class ItemController {
    @Autowired
    private ItemService itemService;

    @Value("${server.port}")
    private  int port;

    @GetMapping("/{orderId}")
    public JsonResult<List<Item>> getItems(@PathVariable("orderId") String orderId) throws InterruptedException {

        log.info("server.port"+port+",orderId ="+orderId);
        //设置延时,为了测试重试效果
        if (Math.random()<0.9){
            long t = new Random().nextInt(5000);
            log.info("延迟"+t);
            Thread.sleep(t);
        }
        List<Item> items = itemService.getItems(orderId);

        return JsonResult.ok(items).msg("port"+port);
    }
    @PostMapping("/decreaseNumber")
    public JsonResult decrease(@RequestBody List<Item> items){
        itemService.decrease(items);
        return  JsonResult.ok();
    }

}
