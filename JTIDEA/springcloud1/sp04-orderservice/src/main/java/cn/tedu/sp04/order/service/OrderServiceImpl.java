package cn.tedu.sp04.order.service;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.Order;
import cn.tedu.sp01.pojo.User;
import cn.tedu.sp01.service.OrderService;
import cn.tedu.sp04.order.feign.ItemFeignClient;
import cn.tedu.sp04.order.feign.UserFeignClient;
import cn.tedu.web.util.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    //注入声明式客户端接口


    @Autowired
    private UserFeignClient userFeignClient;

    @Autowired
    private ItemFeignClient itemFeignClient;

    @Override
    public Order getOrder(String orderId) {
        //TODO: 调用user-service获取用户信息
        JsonResult<User> user = userFeignClient.getUser(7);
        //TODO: 调用item-service获取商品信息
        JsonResult<List<Item>> items = itemFeignClient.getItems(orderId);
        log.info("获取订单,orderId = "+orderId);
        Order order = new Order();
        order.setId(orderId);
        order.setUser(user.getData());
        order.setItems(items.getData());
        return order;
    }

    @Override
    public void addOrder(Order order) {
        //TODO : 新订单中购买的商品,减少商品库存
        itemFeignClient.decrease(order.getItems());
        //TODO: 增加用户的积分
        userFeignClient.addScore(order.getUser().getId(),1000);
      log.info("保存订单:"+order);
    }
}
