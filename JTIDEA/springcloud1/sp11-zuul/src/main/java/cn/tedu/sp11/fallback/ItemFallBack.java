package cn.tedu.sp11.fallback;

import cn.tedu.web.util.JsonResult;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Component
public class ItemFallBack implements FallbackProvider {
    /*
    返回一个servicid 表示针对哪个服务使用当前的降级类
    返回* 或者 null 值 ,表示对所有服务执行当前降级类
     */
    @Override
    public String getRoute() {
        return "item-service";
    }
    /*
    封装降级响应的对象
     */
    @Override
    public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
        return new ClientHttpResponse() {
            @Override
            public HttpStatus getStatusCode() throws IOException {
                return HttpStatus.OK; //封装状态码和状态文本
            }

            @Override
            public int getRawStatusCode() throws IOException {
                return  HttpStatus.OK.value();
            }

            @Override
            public String getStatusText() throws IOException {
                return  HttpStatus.OK.getReasonPhrase();
            }

            @Override
            public void close() {

            }

            @Override
            public InputStream getBody() throws IOException {
                String josn = JsonResult.err().msg("调用远程商品服务失败").toString();
                ByteArrayInputStream inputStream = new ByteArrayInputStream(josn.getBytes("UTF-8"));


                return inputStream;
            }

            @Override
            public HttpHeaders getHeaders() {
                //Content-Type:application/json
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);

                return headers;
            }
        };
    }
}
