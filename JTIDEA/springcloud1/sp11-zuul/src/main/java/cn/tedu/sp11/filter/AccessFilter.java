package cn.tedu.sp11.filter;
//过滤器

import cn.tedu.web.util.JsonResult;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
@Component
public class AccessFilter extends ZuulFilter {
    /*
    filterType 指过滤器类型  pre post routing error
     */
    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }
    /*
    filterOrder 顺序号 指定过滤器插入的位置
    在默认过滤器中，第5个过滤器在上下文对象中添加了service-id,
    在第5个过滤器之后，层能从上下文对象访问到service-id

     */
    @Override
    public int filterOrder() {
        return 6;
    }
    /*
        对当前请求来说,是否要进行过滤,
        如果返回true,则要进行过滤,会执行过滤的run()方法
        如果返回false,跳过过滤代码,继续执行后面的流程
     */
    @Override
    public boolean shouldFilter() {
        //判断用户调用是否是商品服务
        //如果是商品服务,则进行过滤
        //如果不是,则不过滤
        //当前请求的上下文对象
        RequestContext ctx = RequestContext.getCurrentContext();
        //从上下文对象中获取客户端调用的service id
        String serviceId =(String) ctx.get(FilterConstants.SERVICE_ID_KEY);
        return "item-service".equals(serviceId);
    }
    /*
    过滤代码
    他的返回值,在当前版本中,没有启用,返回任何数据都无效
     */
    @Override
    public Object run() throws ZuulException {
        //
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        String token = request.getParameter("token");
        if (StringUtils.isBlank(token)){
            //没有token 则阻止访问
            ctx.setSendZuulResponse(false);

            //直接向客户端发送响应
            //返回JsonResult : { code:400 ,msg :not log in ,data : null}
            ctx.setResponseStatusCode(JsonResult.NOT_LOGIN);
            ctx.setResponseBody(JsonResult.err()
                    .code(JsonResult.NOT_LOGIN)
                    .msg("not login")
                    .toString());
        }


        return null;
    }
}
