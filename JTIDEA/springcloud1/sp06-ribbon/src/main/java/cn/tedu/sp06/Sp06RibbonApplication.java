package cn.tedu.sp06;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
//@EnableDiscoveryClient //可以省略
//@EnableCircuitBreaker
//@SpringBootApplication
@SpringCloudApplication //该注解可以代替以上三个注解
public class Sp06RibbonApplication {

    //创建 RestTemplate 实例，并存入 spring 容器
    @LoadBalanced //负载均衡注解
    @Bean
    public RestTemplate restTemplate(){
        //RestTemplate 中默认的 Factory 实例中，两个超时属性默认是 -1，
        //未启用超时，也不会触发重试
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        //设置连接后台服务器的超时时间
        factory.setConnectTimeout(1000);
        //接收后台响应的超时时间
        factory.setReadTimeout(1000);
        return  new RestTemplate(factory);
    }

    public static void main(String[] args) {
        SpringApplication.run(Sp06RibbonApplication.class, args);
    }

}
