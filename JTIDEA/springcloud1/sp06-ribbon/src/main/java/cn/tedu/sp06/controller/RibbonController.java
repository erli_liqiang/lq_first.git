package cn.tedu.sp06.controller;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.Order;
import cn.tedu.sp01.pojo.User;
import cn.tedu.web.util.JsonResult;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@Slf4j
public class RibbonController {
    @Autowired
    private RestTemplate restTemplate;
    //当调用后台服务失败,会跳转到指定的方法执行降级代码
    @HystrixCommand(fallbackMethod = "getItemsFB")//fallbackMethod 指定降级方法名称
    @GetMapping("/item-service/{orderId}")
    public JsonResult<List<Item>> getItems(@PathVariable String orderId){
        log.info("调用后台商品服务,查询订单商品列表");
        //将localhost:8001 改为item-service ribbon 可以从注册中心获取对象的地址
        //如果配置了多个地址,可以在地址之间来回轮询调用
        JsonResult jsonResult = restTemplate.getForObject("http://item-service/{1}", JsonResult.class, orderId);
        return  jsonResult;
    }
    @HystrixCommand(fallbackMethod = "decreaseNumberFB")
    @PostMapping("/item-service/decreaseNumber")
    public JsonResult decreaseNumber(@RequestBody List<Item> items){
        log.info("调用后台商品服务,减少商品服务");
        JsonResult jsonResult = restTemplate.postForObject("http://item-service/decreaseNumber", items, JsonResult.class);
        return  jsonResult;

    }
    @HystrixCommand(fallbackMethod = "getUserFB")
    @GetMapping("/user-service/{userId}")
    public JsonResult<User> getUser(@PathVariable Integer userId){
        JsonResult jsonResult = restTemplate.getForObject("http://user-service/{1}", JsonResult.class, userId);
        return  jsonResult;
    }
    @HystrixCommand(fallbackMethod = "addScoreFB")
    @GetMapping("/user-service/{userId}/score")
    public JsonResult addScore(@PathVariable Integer userId,Integer score){
        JsonResult jsonResult = restTemplate.getForObject("http://user-service/{1}/score?score={2}", JsonResult.class, userId, score);
        return  jsonResult;
    }
    @HystrixCommand(fallbackMethod = "getOrderFB")
    @GetMapping("/order-service/{orderId}")
    public JsonResult<Order> getOrder(@PathVariable String orderId){
        JsonResult jsonResult = restTemplate.getForObject("http://order-service/{1}", JsonResult.class, orderId);
        return  jsonResult;
    }
    @HystrixCommand(fallbackMethod = "addOrderFB")
    @GetMapping("/order-service")
    public JsonResult addOrder(){
        JsonResult jsonResult = restTemplate.getForObject("http://order-service", JsonResult.class);
        return  jsonResult;
    }

    //降级方法
    public JsonResult<List<Item>> getItemsFB(String orderId){
        return  JsonResult.err().msg("获取订单商品列表失败,请稍后重试");
    }
    public JsonResult decreaseNumberFB(@RequestBody List<Item> items){
        return  JsonResult.err().msg("商品库存减少失败");

    }
    public JsonResult<User> getUserFB(@PathVariable Integer userId){
        return  JsonResult.err().msg("获取用户失败");
    }
    public JsonResult addScoreFB(@PathVariable Integer userId,Integer score){
        return  JsonResult.err().msg("增加用户积分失败");
    }
    public JsonResult<Order> getOrderFB(@PathVariable String orderId){
        return  JsonResult.err().msg("获取订单失败");
    }
    public JsonResult addOrderFB(){
        return  JsonResult.err().msg("添加订单失败");
    }



}
