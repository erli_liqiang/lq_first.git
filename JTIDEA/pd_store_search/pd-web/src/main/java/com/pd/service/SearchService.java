package com.pd.service;

import com.pd.pojo.Item;
import org.apache.solr.client.solrj.SolrServerException;

import java.io.IOException;
import java.util.List;

public interface SearchService {

    //根据关键词查询
    List<Item> search(String key) throws IOException, SolrServerException;
}
