package com.pd.service.impl;

import com.pd.pojo.Item;
import com.pd.service.SearchService;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.solr.SolrAutoConfiguration;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
@Service
public class SearchServiceImpl implements SearchService {
    // SolrAutoConfiguration
    @Autowired
    private SolrClient solrClient;

    @Override
    public List<Item> search(String key) throws IOException, SolrServerException {
        SolrQuery solrQuery = new SolrQuery(key);
        solrQuery.setStart(0);
        solrQuery.setRows(20);
        //json查询结果被封装在QueryResponse
        QueryResponse query = solrClient.query(solrQuery);
        List<Item> items = query.getBeans(Item.class);


        return items;
    }
}
