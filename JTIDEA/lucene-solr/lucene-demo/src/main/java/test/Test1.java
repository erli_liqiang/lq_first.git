package test;

import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class Test1 {

    String[] a = {
            "3, 华为 - 华为电脑, 爆款",
            "4, 华为手机, 旗舰",
            "5, 联想 - Thinkpad, 商务本",
            "6, 联想手机, 自拍神器"
    };

    @Test
    public void test1() throws IOException {
        //存储索引文件的路径
        FSDirectory d = FSDirectory.open(new File("d:/abc/").toPath());

        //lucene提供的中文分词器
        SmartChineseAnalyzer analyzer = new SmartChineseAnalyzer();
        //通过配置对象来指定分词器
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        //索引输出工具
        IndexWriter writer = new IndexWriter(d, config);
        //遍历
        for (String s:a) {
            String[] arr = s.split("\\s*,\\s*");
            //创建文档,文档中包含的是要索引的字段
            Document doc = new Document();
            doc.add(new LongPoint("id", Long.parseLong(arr[0])));
            doc.add(new StoredField("id", Long.parseLong(arr[0])));
            doc.add(new TextField("title", arr[1], Field.Store.YES));
            doc.add(new TextField("sellPoint", arr[2], Field.Store.YES));
            //将文档写入磁盘索引文件
            writer.addDocument(doc);

        }
        //刷新
        writer.flush();
        //关闭
        writer.close();


    }


    @Test
    public  void  test2() throws IOException {


        //存储索引文件的路径
        FSDirectory d = FSDirectory.open(new File("d:/abc/").toPath());
        //创建搜索工具对象
        DirectoryReader reader = DirectoryReader.open(d);
        IndexSearcher searcher = new IndexSearcher(reader);
        //关键词搜索器,我们搜索 "title:华为"
        TermQuery query = new TermQuery(new Term("title", "华为"));
        //执行查询，并返回前20条数据
        TopDocs docs = searcher.search(query, 20);
        //遍历查询到的结果文档并显示
        for (ScoreDoc scoreDoc:
             docs.scoreDocs) {

            Document doc = searcher.doc(scoreDoc.doc);
            System.out.println(doc.get("id")+"---"+scoreDoc.score);
            System.out.println(doc.get("title"));
            System.out.println(doc.get("sellPoint"));
            System.out.println("-----------------");
        }


    }



}
