package cn.tedu.sp04.order.feign;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.web.util.JsonResult;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
@Component
public class ItemFeignClientFB implements  ItemFeignClient {


    @Override
    public JsonResult<List<Item>> getItems(String orderId) {
        //模拟返回缓存数据
        if (Math.random()<0.5){
            List<Item> items = new ArrayList<>();
            items.add(new Item(1,"缓存1",1));
            items.add(new Item(2,"缓存2",8));
            items.add(new Item(3,"缓存3",5));
            items.add(new Item(4,"缓存4",6));
            return  JsonResult.ok().data(items);
        }
        return JsonResult.err().msg("调用商品服务,获取订单商品列表失败");
    }

    @Override
    public JsonResult decrease(List<Item> items) {
        return JsonResult.err().msg("调用商品服务,减少库存失败");
    }
}