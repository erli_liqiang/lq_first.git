package cn.tedu.sp04.order.feign;

import cn.tedu.sp01.pojo.User;
import cn.tedu.web.util.JsonResult;
import org.springframework.stereotype.Component;

@Component
public class UserFeignClientFB implements  UserFeignClient {
    @Override
    public JsonResult<User> getUser(Integer userId) {
        //添加模拟缓存
        if (Math.random()<0.5){

            User user = new User(userId, "用户名" + userId, "密码" + userId);
            return  JsonResult.ok().data(user);
        }
        return JsonResult.err().msg("调用用户服务,获取用户信息失败");
    }

    @Override
    public JsonResult addScore(Integer userId, Integer score) {
        return JsonResult.err().msg("调用用户服务,新增积分失败");
    }
}
