package cn.tedu.sp09.controller;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.Order;
import cn.tedu.sp01.pojo.User;
import cn.tedu.sp09.feign.ItemFeignClient;
import cn.tedu.sp09.feign.OrderFeignClient;
import cn.tedu.sp09.feign.UserFeignClient;
import cn.tedu.web.util.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class FeignClientController {
    @Autowired
    private ItemFeignClient itemFeignClient;
    @Autowired
    private UserFeignClient userFeignClient;
    @Autowired
    private OrderFeignClient orderFeignClient;

    @GetMapping("/item-service/{orderId}")
    public JsonResult<List<Item>> getItems(@PathVariable String orderId){
        log.info("调用远程商品服务,获取订单列表");
        JsonResult<List<Item>> items = itemFeignClient.getItems(orderId);
        return  items;
    }
    @PostMapping("/item-service/decreaseNumber")
    public  JsonResult decreaseNumber(List<Item> items){
        log.info("调用远程商品服务,减少库存");
        return  itemFeignClient.decrease(items);
    }
    @GetMapping("/user-service/{userId}")
    public JsonResult<User> getUser(@PathVariable  Integer userId){
        log.info("调用远程用户服务,获取用户信息");
        return userFeignClient.getUser(userId);
    }

    @GetMapping("/user-service/{userId}/score")
    public  JsonResult addScore(@PathVariable Integer userId,Integer score){
        log.info("调用远程用户服务,新增积分");
        return  userFeignClient.addScore(userId,score);
    }
    @GetMapping("/order-service/{orderId}")
    public JsonResult<Order> getOrder(@PathVariable  String orderId){
        log.info("调用远程订单服务,获取订单");
        return orderFeignClient.getOrder(orderId);
    }

    @GetMapping("/order-service")
    public  JsonResult addOrder(){
        log.info("调用远程订单服务,新增订单");
        return  orderFeignClient.addOrder();
    }


}
