package cn.tedu.sp03.user.service;

import cn.tedu.sp01.pojo.User;
import cn.tedu.sp01.service.UserService;
import cn.tedu.web.util.JsonUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.sun.xml.internal.bind.v2.TODO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Value("${sp.user-service.users}")
    private String userJson;

    @Override
    public User getUser(Integer userId) {
        log.info("user json String "+userJson);
        List<User> users = JsonUtil.from(userJson, new TypeReference<List<User>>() {
        });
        for (User user : users) {
            if (user.getId().equals(userId))
                return  user;
        }

        return new User(userId,"用户"+userId,"密码"+userId);
    }

    @Override
    public void addScore(Integer userId, Integer score) {
        //TODO 这里增加积分
        log.info("user "+userId+" - 增加积分 "+score);
    }
}
